from ._pipeline import PIPELINE


PIPELINE['STYLESHEETS'].update({
    'critical': {
        'source_filenames': (
            'scss/grid.scss',
            'scss/layout.scss',
            'scss/buttons.scss',

            'header/scss/header.scss',
            'menu/scss/main_menu.scss',
        ),
        'output_filename': 'css_build/critical.css',
    },
    'core': {
        'source_filenames': (
            'css/jquery-ui.css',
            'scss/datetimepicker.scss',
            'scss/forms.scss',
            'scss/preloader.scss',
            'scss/text_styles.scss',

            'scss/custom_form.scss',
            'scss/popups/popups.scss',
            'scss/popups/preloader.scss',

            'css/slick.css',
            'scss/woco-accordion.scss',
            'scss/jquery.tooltip.scss',

            'subscriptions/scss/popups.scss',
            'footer/scss/footer.scss',

        ),
        'output_filename': 'css_build/head_core.css',
    },
    'error': {
        'source_filenames': (
            'scss/error_page.scss',
        ),
        'output_filename': 'css_build/error.css',
    },
    'politics': {
        'source_filenames': (
            'scss/politics.scss',
        ),
        'output_filename': 'css_build/politics.css',
    },
    'clients': {
        'source_filenames': (
            'clients/scss/clients.scss',
            # 'portal/scss/index.scss',
        ),
        'output_filename': 'css_build/clients.css',
    },
    'portal': {
        'source_filenames': (
            'portal/scss/header.scss',
            'portal/scss/index.scss',
            'portal/scss/popups/submit_injury.scss',
            'clients/scss/profile.scss',
            'subscriptions/scss/popups/subscription.scss',
            'packages/scss/popups/package.scss',
            'clients/scss/popups.scss',
        ),
        'output_filename': 'css_build/portal.css',
    },
    'profile': {
        'source_filenames': (
            'portal/scss/header.scss',
            'portal/scss/index.scss',
            'portal/scss/popups/submit_injury.scss',
            'clients/scss/profile.scss',
            'scss/custom_form.scss',
            'subscriptions/scss/popups/subscription.scss',
            'packages/scss/popups/package.scss',
            'clients/scss/popups.scss',
        ),
        'output_filename': 'css_build/profile.css',
    },
    'fonts': {
        'source_filenames': (
            'fonts/OpenSans-Regular/stylesheet.css',
            'fonts/OpenSans-SemiBold/stylesheet.css',
            'fonts/OpenSans-Bold/stylesheet.css',
            'fonts/OpenSans-ExtraBold/stylesheet.css',
            'fonts/Raleway-Regular/stylesheet.css',
            'fonts/Raleway-Black/stylesheet.css',
            'fonts/Raleway-Bold/stylesheet.css',
            'fonts/Raleway-ExtraBold/stylesheet.css',
        ),
        'output_filename': 'css_build/fonts.css',
    },
    'main': {
        'source_filenames': (
            'scss/animations.scss',
            'main/scss/index.scss',
            'contacts/scss/block.scss',
            'subscriptions/scss/popups/subscription.scss',
        ),
        'output_filename': 'css_build/main.css',
    },
    'users': {
        'source_filenames': (
            # 'scss/animations.scss',
            'scss/users.scss',
            'scss/profile.scss',
            'scss/submit.scss',
        ),
        'output_filename': 'css_build/users.css',
    },
    'contacts': {
        'source_filenames': (
            'scss/formstyler.css',
            'scss/formstyler.theme.css',
            'google_maps/scss/label.scss',
        ),
        'output_filename': 'css_build/contacts.css',
    },
})

PIPELINE['JAVASCRIPT'].update({
    'core': {
        'source_filenames': (
            'polyfills/modernizr.js',
            'js/jquery-2.2.4.js',
            'js/jquery-ui.js',
            'js/jquery.requestanimationframe.js',

            'js/jquery.cookie.js',
            'js/jquery.session.js',
            'common/js/jquery.utils.js',
            'common/js/jquery.ajax_csrf.js',

            'js/popups/jquery.popups.js',
            'js/preload_main.js',
            'js/popups/preloader.js',
            'js/jquery.inspectors.js',
            'js/jquery.scrollTo.js',
            'js/jquery.fitvids.js',
            'js/text_styles.js',
            'js/sticky.js',
            'js/slick.js',
            'js/lazyload.js',
            'js/woco.accordion.js',
            'js/jquery.tooltip.js',

            'attachable_blocks/js/async_blocks.js',
            'placeholder/js/placeholder.js',
            'subscriptions/js/popups.js',
            'menu/js/main_menu.js',
        ),
        'output_filename': 'js_build/core.js',
    },
    'main': {
        'source_filenames': (
            'js/captcha.js',
            'main/js/index.js',
            'contacts/js/forms.js',
            'purchase/js/purchase.js',
            'clients/js/popups.js',
            'subscriptions/js/subscription.js',
        ),
        'output_filename': 'js_build/main.js',
    },
    'clients': {
        'source_filenames': (
            'clients/js/clients.js',
        ),
        'output_filename': 'js_build/clients.js',
    },
    'users': {
        'source_filenames': (
            'users/js/popup.js',
            'users/js/profile.js',
        ),
        'output_filename': 'js_build/users.js',
    },
    'portal': {
        'source_filenames': (
            'portal/js/portal.js',
            'portal/js/tabs.js',
            'portal/js/popups.js',
            'subscriptions/js/subscription.js',
            'packages/js/popups.js',
        ),
        'output_filename': 'js_build/portal.js',
    },
    'profile': {
        'source_filenames': (
            'common/js/plupload/plupload.full.min.js',
            'common/js/uploader.js',
            'portal/js/popups.js',
            'clients/js/profile.js',
            'clients/js/tabs.js',
            'subscriptions/js/subscription.js',
            'packages/js/popups.js',
        ),
        'output_filename': 'js_build/profile.js',
    },
})

