(function ($) {
    window.recaptcha_register = false;
    window.recaptcha_contact = false;

    window.sendMessage = function(token) {

        return new Promise(function(resolve, reject) {
            window.token = token;
            window.form.trigger('submit');
        })
    };

    window.onloadCallback = function() {
        window.captchaOptions = {
            sitekey: document.documentElement.getAttribute('data-google-recaptcha-key'),
            size: 'invisible',
            callback: window.sendMessage
        };
        var contactCaptchaln = ($('#contact_recaptcha').length);

        if(contactCaptchaln)
            window.recaptcha_contact = window.grecaptcha.render('contact_recaptcha', window.captchaOptions);
    };
})(jQuery);