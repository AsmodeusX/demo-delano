(function($) {


    $(window).on('load', function () {
        var $preloader = $('.page-loading'),
            $spinner = $preloader.find('.spinner');
        $spinner.fadeOut();
        $preloader.delay(350).fadeOut('slow').dequeue();

        if ($.winWidth() > 1200) {
            $('.fon-red').addClass('background-animation-initial');
            $('.logo-box').addClass('logo-animate fadeInDown');
            $('.title-h1-main, .phone-wrapper').addClass('title-animate fadeInDown');
            $('.home-wrapper, .btn-hero-white, .btn-hero-red').addClass('btn-animate fadeInDown');
            $('.title-desc, .text-main, .prices-title, .text-price-top, .text-price-bottom').addClass('title-animate-up fadeInUp');
            $('header').addClass('header-after');
        }
    });

})(jQuery);
