# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_networks', '0007_auto_20170121_0848'),
    ]

    operations = [
        migrations.AddField(
            model_name='sociallinks',
            name='social_linkedin',
            field=models.URLField(verbose_name='linkedin', blank=True, max_length=255),
        ),
    ]
