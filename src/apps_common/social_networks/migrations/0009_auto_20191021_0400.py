# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_networks', '0008_sociallinks_social_linkedin'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sociallinks',
            name='social_instagram',
        ),
        migrations.AddField(
            model_name='sociallinks',
            name='social_youtube',
            field=models.URLField(verbose_name='youtube', max_length=255, blank=True),
        ),
    ]
