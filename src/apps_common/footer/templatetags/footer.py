from django.template import Library, loader
from .. import conf
from contacts.models import ContactsConfig
from politics.models import Terms, Privacy

register = Library()


@register.simple_tag(takes_context=True)
def footer(context, template='footer/footer.html'):
    """ Футер """
    return loader.render_to_string(template, {
        'privacy': Privacy.get_solo(),
        'terms': Terms.get_solo(),
        'contacts': ContactsConfig.get_solo(),
    }, request=context.get('request'))


@register.simple_tag(takes_context=True)
def portal_footer(context, template='footer/portal_footer.html'):
    return loader.render_to_string(template, {
        'privacy': Privacy.get_solo(),
        'terms': Terms.get_solo(),
        'contacts': ContactsConfig.get_solo(),
    }, request=context.get('request'))


@register.simple_tag(takes_context=True)
def dl_link(context, template='footer/dl_link.html'):
    request = context.get('request')
    if not request:
        return ''

    rule = conf.RULES.get(request.path_info)
    if rule:
        return loader.render_to_string(template, rule, request=request)

    return loader.render_to_string(template, {
        'url': 'https://directlinedev.com/',
        'title': 'Web Development',
        'fallback': True,
    }, request=request)
