import paypalrestsdk
from django.shortcuts import resolve_url
from . import conf
import logging
from .new_subscription_api import new_subscription


logger = logging.getLogger('django.paypal')

# New

def define_config():
    return paypalrestsdk.configure({
        'mode': conf.API_MODE,
        'client_id': conf.CLIENT_ID,
        'client_secret': conf.SECRET
    })


def define_payment(request, intent='sale', payment_method='paypal', amount='0.00',
                   currency='', description='', return_url=None, cancel_url=None, payment_id=None):

    return_url = return_url or request.build_absolute_uri(resolve_url(conf.PAYMENT_RESULT_URL))
    cancel_url = cancel_url or request.build_absolute_uri(resolve_url(conf.PAYMENT_CANCEL_URL))

    payment = paypalrestsdk.Payment({
        'intent': intent,

        # Set payment method
        'payer': {
            'payment_method': payment_method
        },

        # Set redirect URLs
        'redirect_urls': {
            'return_url': return_url,
            'cancel_url': cancel_url
        },

        # Set transaction object
        'transactions': [{
            'invoice_number': str(payment_id),
            'amount': {
                'total': amount,
                'currency': currency or conf.CURRENCY
            },
            'description': description
        }]
    })

    return payment


def define_product(name='', description='', product_type='',
                   image_url='', home_url=''):

    if len(name) < 1:
        logger.error('Short product name')

    product_info = {
        'name': name[:127],
        'type': product_type or conf.PRODUCT_TYPE_PHYSICAL,
        # 'category': name,
    }

    if description:
        product_info['description'] = description[:256]

    if image_url:
        product_info['image_url'] = image_url

    if home_url:
        product_info['home_url'] = home_url

    return new_subscription.Product(product_info)


def get_product(product_id):
    if not product_id:
        logger.error('Paypal get product, ID is required')
        return

    return new_subscription.Product.find(product_id)


def update_product(product=None, attributes=None, refresh_token=None):
    if not product:
        logger.error('Paypal update product, object is required')
        return

    return product.update(attributes=attributes, refresh_token=refresh_token)


def define_plan(product_id='', name='Plan with regular payment definition',
                description='', status=None, currency='', initial_fee='0.00',
                regular_frequency=conf.SUBSCRIPTION_PLAN_FREQUENCY_MONTH,
                regular_frequency_interval=1,
                regular_amount='0.00',
                regular_cycles=998,
                # trial_frequency=conf.SUBSCRIPTION_PLAN_FREQUENCY_MONTH,
                # trial_frequency_interval=1,
                # trial_amount='0.00',
                # trial_cycles=998
                ):

    if not product_id:
        logger.error('Product ID is required')

    if len(name) < 1:
        logger.error('Short plan name')

    description = description or name

    regular_interval_count = min(
        conf.SUBSCRIPTION_FREQUENCY_MAX_INTERVAL_LENGTH[regular_frequency],
        int(regular_frequency_interval)
    )
    # trial_interval_count = min(
    #     conf.SUBSCRIPTION_FREQUENCY_MAX_INTERVAL_LENGTH[trial_frequency],
    #     int(trial_frequency_interval)
    # )

    plan = new_subscription.Plan({
        'product_id': product_id,
        'name': name[:127],
        'status': status or conf.SUBSCRIPTION_PLAN_STATUS_ACTIVE,
        'description': description[:127],
        'billing_cycles': [
            {
                'frequency': {
                    'interval_unit': regular_frequency,
                    'interval_count': regular_interval_count
                },
                'tenure_type': 'REGULAR',
                'sequence': 1, # если есть триал то тут 2
                'total_cycles': max(1, min(998, int(regular_cycles))),
                'pricing_scheme': {
                    'fixed_price': {
                        'currency_code': currency or conf.CURRENCY,
                        'value': regular_amount
                    }
                }
            },
            # {
            #     'frequency': {
            #         'interval_unit': trial_frequency,
            #         'interval_count': trial_interval_count
            #     },
            #     'tenure_type': 'TRIAL',
            #     'sequence': 1,
            #     'total_cycles': max(1, min(999, int(trial_cycles))),
            #     'pricing_scheme': {
            #         'fixed_price': {
            #             'currency_code': currency or conf.CURRENCY,
            #             'value': trial_amount or Decimal('0.00')
            #         }
            #     }
            # }
        ],
        'payment_preferences': {
            'auto_bill_outstanding': True,
            'setup_fee': {
                'currency_code': currency or conf.CURRENCY,
                'value': initial_fee
            },
            'setup_fee_failure_action': 'CONTINUE',
            'payment_failure_threshold': 3
        },
        # 'taxes': {
        #     'percentage': '10',
        #     'inclusive': True,
        # },
        # 'quantity_supported': False
    })

    return plan


def get_plan(plan_id):
    if not plan_id:
        logger.error('Paypal get plan, ID is required')
        return

    return new_subscription.Plan.find(plan_id)


def define_subscription(plan_id='', start_date=None, quantity=1, email='',
                        return_url=None, cancel_url=None, amount='0.00', **kwargs):

    if not plan_id:
        logger.error('Plan ID is required')

    if not email:
        logger.error('Email is required')

    if not return_url:
        raise ValueError('Return url is required')

    if not cancel_url:
        raise ValueError('Cancel url is required')


    application_context = {
        # 'brand_name': kwargs.get('brand_name', '')[:127],
        'locale': kwargs.get('locale', 'en-US')[:10],
        'shipping_preference': kwargs.get('shipping_preference', conf.SUBSCRIPTION_SHIPPING_ABSENCE)[:24],
        'user_action': kwargs.get('user_action', conf.SUBSCRIPTION_USER_ACTION_SUBSCRIBE_NOW)[:24],
        'payment_method': {
            'payer_selected': kwargs.get('payer_selected', conf.SUBSCRIPTION_PAYER_PAYPAL),
            'payee_preferred': kwargs.get('payee_preferred', conf.SUBSCRIPTION_PAYEE_UNRESTRICTED)
        },
        'return_url': return_url[:4000],
        'cancel_url': cancel_url[:4000]
    }
    subscription = new_subscription.Subscription({
        'plan_id': plan_id,
        # 'start_time': now_to_rfc3339() if start_time is None else start_time,
        'start_time': str(start_date),
        'quantity': min(32, max(1, int(quantity))),
        'shipping_amount': {
            'currency_code': kwargs.get('currency', conf.CURRENCY),
            'value': amount,
        },
        'subscriber': {
            'name': {
                'given_name': kwargs.get('first_name', '')[:140],
                'surname': kwargs.get('last_name', '')[:140]
            },
            'email_address': email[:254],
            # 'shipping_address': shipping_address
        },
        'auto_renewal': False,
        'application_context': application_context
    })

    return subscription


def get_subscription(subscription_id):
    if not subscription_id:
        logger.error('Paypal get subscription, ID is required')
        return

    try:
        return new_subscription.Subscription.find(subscription_id)
    except:
        return