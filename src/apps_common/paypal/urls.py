from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^payment-result/$', views.payment_result, name='payment_result'),
    url(r'^subscription-result/$', views.subscription_agreement_result, name='subscription_result'),
    url(r'^webhooks/status/$', views.webhook_status, name='webhook_status'),
    url(r'^webhooks/transactions/$', views.webhook_transactions, name='webhook_transactions'),
]
