# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators
import libs.valute_field.fields


class Migration(migrations.Migration):

    dependencies = [
        ('paypal', '0002_product'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubscriptionPlan',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('plan_id', models.CharField(max_length=64, verbose_name='Paypal plan ID', editable=False)),
                ('amount', libs.valute_field.fields.ValuteField(editable=False, default=10, verbose_name='Amount', validators=[django.core.validators.MinValueValidator(1)])),
                ('frequency_interval', models.PositiveSmallIntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(12)], editable=False, default=12, verbose_name='Frequency interval', help_text='months')),
                ('cycles', models.PositiveSmallIntegerField(editable=False, default=0, verbose_name='Number of payments', help_text='Zero means endless subscription')),
                ('created', models.DateTimeField(editable=False, blank=True, null=True, verbose_name='Create date')),
            ],
            options={
                'verbose_name': 'Subscription plan',
                'ordering': ('-created', '-amount'),
                'verbose_name_plural': 'Subscription plans',
            },
        ),
        migrations.RemoveField(
            model_name='product',
            name='plan_id',
        ),
        migrations.AddField(
            model_name='product',
            name='product_id',
            field=models.CharField(max_length=64, blank=True, editable=False, verbose_name='Paypal product ID'),
        ),
    ]
