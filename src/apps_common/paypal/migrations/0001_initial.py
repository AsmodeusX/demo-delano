# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('inv_id', models.BigIntegerField(null=True, verbose_name='invoice', blank=True)),
                ('status', models.PositiveSmallIntegerField(choices=[(1, 'Info'), (2, 'Success'), (3, 'Error'), (4, 'Exception')], verbose_name='status')),
                ('msg_body', models.TextField(verbose_name='message')),
                ('request_get', models.TextField(verbose_name='GET')),
                ('request_post', models.TextField(verbose_name='POST')),
                ('request_ip', models.GenericIPAddressField(verbose_name='IP')),
                ('created', models.DateTimeField(editable=False, default=django.utils.timezone.now, verbose_name='create date')),
            ],
            options={
                'verbose_name': 'log message',
                'default_permissions': ('delete',),
                'ordering': ('-created',),
                'verbose_name_plural': 'log messages',
            },
        ),
    ]
