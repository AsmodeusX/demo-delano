from django.utils import timezone
import datetime


def datetimetostr(dt):
    """
    Return a RFC3339 date-time string corresponding to the given
    datetime object. Special-case both absent timezone and timezone
    offset zero to use 'Z' instead of '+00:00'.
    >>> datetimetostr(datetime.datetime(2008, 8, 24, 0, 0, tzinfo=UTC_TZ))
    '2008-08-24T00:00:00Z'
    >>> datetimetostr(datetime.datetime(2008, 8, 24, 0, 0))
    '2008-08-24T00:00:00Z'
    >>> datetimetostr(datetime.datetime(2008, 8, 24, 0, 0, tzinfo=tzinfo(60, '+01:00')))
    '2008-08-24T00:00:00+01:00'
    >>> datetimetostr(datetime.datetime(2008, 8, 24, 0, 0, 11, 250000, tzinfo=tzinfo(-83, '-01:23')))
    '2008-08-24T00:00:11.250000-01:23'
    """

    if timezone.is_naive(dt):
        return "%sZ" % dt.isoformat()
        # return dt.strftime('%Y-%m-%dT%H:%M:%SZ')

    if dt.utcoffset() == timezone.ZERO:
        return "%sZ" % dt.replace(tzinfo=None).isoformat()
        # return dt.replace(tzinfo=None).strftime('%Y-%m-%dT%H:%M:%SZ')

    return dt.isoformat()


def now_to_rfc3339():
    return datetimetostr(timezone.now())


def get_localtime(ts):
    ts_dt_utc = timezone.make_aware(timezone.datetime.strptime(ts, "%Y-%m-%dT%H:%M:%SZ"), timezone=timezone.utc)
    # ts_dt_utc = timezone.make_aware(timezone.datetime.strptime(ts, "%Y-%m-%dT%H:%M:%S.%fZ"), timezone=timezone.utc)
    return timezone.localtime(ts_dt_utc)



def get_date(ts):
    ts_dt_utc = timezone.make_aware(timezone.datetime.strptime(ts, "%Y-%m-%dT%H:%M:%SZ"), timezone=timezone.utc)
    # ts_dt_utc = timezone.make_aware(timezone.datetime.strptime(ts, "%Y-%m-%dT%H:%M:%S.%fZ"), timezone=timezone.utc)
    return timezone.datetime.date(ts_dt_utc)
