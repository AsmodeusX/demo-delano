"""
    Модуль оплаты через PayPal.

    Зависит от:
        paypalrestsdk

    Установка:
        settings.py:
            INSTALLED_APPS = (
                ...
                'paypal',
                ...
            )

            PAYPAL_TEST_MODE = True
            PAYPAL_CLIENT_ID = 'AWE-kvm07Uw3qbpXgm_CTitwtnB7ujtxeAGdVcJprwVdEiRu69tvWorAvWBwygNVhD8GpM2Kjxa-AeYk'
            PAYPAL_SECRET = 'EM4JtPXvdUnEw4N6qKBnH049wjD0d_CrZKY0ihCtQ50fH-gVUF50UsUkqpE6y4vC4qyA9HMvzPDCzX31'
            PAYPAL_CURRENCY = 'USD'
            PAYPAL_PAYMENT_SUCCESS_URL = 'shop:index'
            PAYPAL_PAYMENT_CANCEL_URL = 'shop:index'

            SUIT_CONFIG = {
                ...
                {
                    'app': 'paypal',
                    'icon': 'icon-shopping-cart',
                },
                ...
            }

        urls.py:
            ...
            url(r'^paypal/', include('paypal.urls', namespace='paypal')),
            ...

"""

default_app_config = 'paypal.apps.Config'
