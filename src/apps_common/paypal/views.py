from django.shortcuts import redirect, resolve_url, Http404, get_object_or_404
from django.http.response import HttpResponseForbidden, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from subscriptions.models import AdditionalHours, Transaction, Subscription
from . import conf
from subscriptions.models import Purchase
from clients.views import get_client
from django.views.decorators.http import require_POST
from .api import get_subscription
import json
import logging
import paypalrestsdk


def _log_errors(errors):
    return '\n'.join(
        '{}: {}'.format(
            key,
            ', '.join(errors_list)
        )
        for key, errors_list in errors.items()
    )


@csrf_exempt
def payment_result(request):
    """ Обработчик результата оплаты """
    payment_id = request.GET.get('paymentId')
    payer_id = request.GET.get('PayerID')

    if payment_id is None or payer_id is None:
        return HttpResponseForbidden()

    paypalrestsdk.configure({
        'client_id': conf.CLIENT_ID,
        'client_secret': conf.SECRET,
    })

    payment = paypalrestsdk.Payment.find(payment_id)

    for transaction in payment.transactions:

        purchase = get_object_or_404(Purchase, id=transaction.invoice_number)

        purchase.name = transaction.item_list.shipping_address.recipient_name
        purchase.email = transaction.payee.email
        purchase.save()

    redirect_page = resolve_url('portal:index')
    client = get_client(request)

    hours = AdditionalHours.objects.filter(subscription__client_id=client.id).first()
    hours.status = str(payment.state).upper()
    hours.payment_id = payment.id
    hours.save()

    return redirect(redirect_page)


@csrf_exempt
def subscription_agreement_result(request):
    """ Обработчик результата подписки """
    subscription_id = request.GET.get('subscription_id')

    if subscription_id is None:
        raise Http404

    paypalrestsdk.configure({
        'client_id': conf.CLIENT_ID,
        'client_secret': conf.SECRET,
    })

    subscription_obj = get_subscription(subscription_id)

    client = get_client(request)

    if not client:
        raise Http404

    subscription = client.subscription_client.last()

    purchase = get_object_or_404(Purchase, subscription_id=subscription.id)

    if subscription_obj:
        # Subscription.objects.filter(id=subscription.id).update(
        #     subscription_id=subscription_obj.id
        # ).save()

        subscription.subscription_id = subscription_obj.id
        subscription.save()
        purchase.name = '{} {}'.format(subscription_obj.subscriber.name.given_name,

        subscription_obj.subscriber.name.surname)
        purchase.email = subscription_obj.subscriber.email_address
        purchase.save()

        redirect_page = resolve_url('portal:index')

        return redirect(redirect_page)
    else:
        purchase.payment_complete = False
        purchase.save()
        return redirect(resolve_url('index'))

#
@csrf_exempt
@require_POST
def webhook_status(request):
    jsondata = request.body
    data = json.loads(jsondata.decode('UTF-8'))

    resource = data.get('resource')

    if resource:
        status = resource.get('status')
        subscription_id = resource.get('id')

        try:
            subscription = Subscription.objects.get(subscription_id=subscription_id)
            subscription.status = str(status).upper()
            subscription.save()

        except Exception as e:
            logging.error(e)

    return HttpResponse(status=200)


@csrf_exempt
@require_POST
def webhook_transactions(request):
    jsondata = request.body
    data = json.loads(jsondata.decode('UTF-8'))
    resource = data.get('resource')

    if resource:

        billing_agreement_id = resource.get('billing_agreement_id')
        if billing_agreement_id:
            try:
                purchase = Purchase.objects.get(subscription__subscription_id=billing_agreement_id)
                amount = resource.get('amount')
                if amount:
                    amount = amount.get('total')

                if amount:
                    Transaction(
                        price=amount,
                        purchase_id=purchase.id
                    ).save()
            except Exception as e:
                logging.error(e)
        else:
            payment_id = resource.get('id')

            if payment_id:
                purchase = Purchase.objects.get(additional_hours__payment_id=payment_id)
                transactions = resource.get('transactions')

                if transactions:
                    for transaction in transactions:
                        if transactions:
                            amount = transaction.get('amount')

                            amount = amount.get('total')

                            if amount:
                                Transaction(
                                    price=amount,
                                    purchase_id=purchase.id
                                ).save()
    return HttpResponse(status=200)