import paypalrestsdk.util as util
from datetime import datetime
from paypalrestsdk.resource import List, Find, Create, Post, Update, Replace, Resource
from paypalrestsdk.api import default as default_api
from paypalrestsdk import exceptions
from ..utils import datetimetostr


class Product(List, Create, Find, Replace):
    """
    https://developer.paypal.com/docs/api/catalog-products/v1/
    """
    path = "v1/catalogs/products"

    def update(self, attributes=None, refresh_token=None):
        attributes = attributes or self.to_dict()
        url = util.join_url(self.path, str(self['id']))
        new_attributes = self.api.patch(url, attributes, self.http_headers(), refresh_token)
        self.error = None
        self.merge(new_attributes)
        return self.success()

Product.convert_resources['product'] = Product
Product.convert_resources['products'] = Product


class Plan(List, Create, Find, Replace, Post):
    path = "/v1/billing/plans"

    def activate(self):
        return self.post('activate', {}, self)

    def deactivate(self):
        return self.post('deactivate', {}, self)

    def update_pricing_schemes(self, cycle_sequence, value='0.00', currency_code='USD'):
        update_attributes = {
            'pricing_schemes': [{
                'billing_cycle_sequence': max(1, min(99, cycle_sequence)),
                'pricing_scheme': {
                    'fixed_price': {
                        'value': value,
                        'currency_code': currency_code
                    }
                }
            }]
        }
        return self.post('update-pricing-schemes', update_attributes, self)

Plan.convert_resources['plan'] = Plan
Plan.convert_resources['plans'] = Plan


class Subscription(List, Create, Find, Replace, Post):
    path = "/v1/billing/subscriptions"

    def activate(self, attributes):
        if isinstance(attributes, dict):
            attributes.update({'reason': 'Reactivating the subscription'})
        else:
            attributes = {'reason': 'Reactivating the subscription'}
        return self.post('activate', attributes, self)

    def suspend(self, attributes):
        if isinstance(attributes, dict):
            attributes.update({'reason': 'Item out of stock'})
        else:
            attributes = {'reason': 'Item out of stock'}
        return self.post('suspend', attributes, self)

    def cancel(self, attributes):
        if isinstance(attributes, dict):
            attributes.update({'reason': 'Not satisfied with the service'})
        else:
            attributes = {'reason': 'Not satisfied with the service'}

        return self.post('cancel', attributes, self)

    def get_approve_url(self):
        if self.status == 'APPROVAL_PENDING':
            for link in self.links:
                if link.rel == "approve":
                    return str(link.href)

    def search_transactions(self, start_time, end_time, api=None):
        if not start_time or not end_time:
            raise exceptions.MissingParam("Search transactions needs valid start_time and end_time.")
        api = api or default_api()

        # Construct url similar to
        # /billing-agreements/I-HT38K76XPMGJ/transactions?start_time=2014-04-13&end_time=2014-04-30
        endpoint = util.join_url(self.path, str(self['id']), 'transactions')
        start_time = datetimetostr(start_time) if isinstance(start_time, datetime) else start_time
        end_time = datetimetostr(end_time) if isinstance(end_time, datetime) else end_time
        date_range = [('start_time', start_time), ('end_time', end_time)]

        url = util.join_url_params(endpoint, date_range)
        return Resource(self.api.get(url), api=api)

Subscription.convert_resources['subscription'] = Subscription
Subscription.convert_resources['subscriptions'] = Subscription
