"""
    Обновление апи для подписки paypalrestsdk

    https://developer.paypal.com/docs/subscriptions/integrate/#set-up-your-development-environment

    1. Create Product
    2. Create Plan
    3. Create Subscription
"""
from .new_subscription import Product, Plan, Subscription
