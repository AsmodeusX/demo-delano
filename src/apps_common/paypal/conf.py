from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# ================
# General
# ================

# Обязательные параметры
EMAIL = settings.PAYPAL_EMAIL
CLIENT_ID = settings.PAYPAL_CLIENT_ID
SECRET = settings.PAYPAL_SECRET
CURRENCY = settings.PAYPAL_CURRENCY

# Включен ли тестовый режим
TEST_MODE = getattr(settings, 'PAYPAL_TEST_MODE', False)

if TEST_MODE:
    API_MODE = 'sandbox'
else:
    API_MODE = 'live'


# ================
# Payment
# ================

# Адрес страницы обработки результата
PAYMENT_RESULT_URL = getattr(
    settings,
    'PAYPAL_PAYMENT_RESULT_URL',
    'paypal:payment_result'
)

# Адрес, куда будет перенаправлен пользователь после успешной оплаты
PAYMENT_SUCCESS_URL = getattr(
    settings,
    'PAYPAL_PAYMENT_SUCCESS_URL',
    '/'
)

# Адрес, куда будет перенаправлен пользователь после неудачной оплаты
PAYMENT_CANCEL_URL = getattr(
    settings,
    'PAYPAL_PAYMENT_CANCEL_URL',
    '/'
)


# ================
# Billing
# ================

# Billing plan type
BILLING_PLAN_FIXED = 'FIXED'
BILLING_PLAN_INFINITE = 'INFINITE'

# Billing plan frequency
BILLING_PLAN_FREQUENCY_DAY = 'DAY'
BILLING_PLAN_FREQUENCY_WEEK = 'WEEK'
BILLING_PLAN_FREQUENCY_MONTH = 'MONTH'
BILLING_PLAN_FREQUENCY_YEAR = 'YEAR'

# Адрес страницы обработки подтверждения подписки
BILLING_AGREEMENT_RESULT_URL = getattr(
    settings,
    'PAYPAL_BILLING_AGREEMENT_RESULT_URL',
    'paypal:billing_agreement_result'
)

# Адрес, куда будет перенаправлен пользователь после успешной подписки
BILLING_AGREEMENT_SUCCESS_URL = getattr(
    settings,
    'PAYPAL_BILLING_AGREEMENT_SUCCESS_URL',
    '/'
)

# Адрес, куда будет перенаправлен пользователь для отмены подписки
BILLING_AGREEMENT_CANCEL_URL = getattr(
    settings,
    'PAYPAL_BILLING_AGREEMENT_CANCEL_URL',
    '/'
)
# plan frequency
SUBSCRIPTION_PLAN_FREQUENCY_DAY = 'DAY'
SUBSCRIPTION_PLAN_FREQUENCY_WEEK = 'WEEK'
SUBSCRIPTION_PLAN_FREQUENCY_SEMI_MONTH = 'SEMI_MONTH'
SUBSCRIPTION_PLAN_FREQUENCY_MONTH = 'MONTH'
SUBSCRIPTION_PLAN_FREQUENCY_YEAR = 'YEAR'
SUBSCRIPTION_PLAN_FREQUENCY_CHOICES = (
    (SUBSCRIPTION_PLAN_FREQUENCY_DAY, _('Day')),
    (SUBSCRIPTION_PLAN_FREQUENCY_WEEK, _('Week')),
    (SUBSCRIPTION_PLAN_FREQUENCY_SEMI_MONTH, _('Semi month')),
    (SUBSCRIPTION_PLAN_FREQUENCY_MONTH, _('Month')),
    (SUBSCRIPTION_PLAN_FREQUENCY_YEAR, _('Year')),
)

SUBSCRIPTION_FREQUENCY_MAX_INTERVAL_LENGTH = {
    SUBSCRIPTION_PLAN_FREQUENCY_DAY: 365,
    SUBSCRIPTION_PLAN_FREQUENCY_WEEK: 52,
    SUBSCRIPTION_PLAN_FREQUENCY_SEMI_MONTH: 12,
    SUBSCRIPTION_PLAN_FREQUENCY_MONTH: 12,
    SUBSCRIPTION_PLAN_FREQUENCY_YEAR: 1,
}


# ================
# Subscription
# ================

# plan status
SUBSCRIPTION_PLAN_STATUS_CREATED = 'CREATED'
SUBSCRIPTION_PLAN_STATUS_INACTIVE = 'INACTIVE'
SUBSCRIPTION_PLAN_STATUS_ACTIVE = 'ACTIVE'
SUBSCRIPTION_PLAN_STATUS_CHOICES = (
    (SUBSCRIPTION_PLAN_STATUS_CREATED, _('Created')),
    (SUBSCRIPTION_PLAN_STATUS_INACTIVE, _('Inactive')),
    (SUBSCRIPTION_PLAN_STATUS_ACTIVE, _('Active')),
)

# Адрес страницы обработки подтверждения подписки
SUBSCRIPTION_RESULT_URL = getattr(
    settings,
    'PAYPAL_SUBSCRIPTION_RESULT_URL',
    'paypal:subscription_result'
)

# Адрес, куда будет перенаправлен пользователь для отмены подписки
SUBSCRIPTION_CANCEL_URL = getattr(
    settings,
    'PAYPAL_SUBSCRIPTION_CANCEL_URL',
    'users:profile_self'
)

# статусы подписки
SUBSCRIPTION_APPROVAL_PENDING = 'APPROVAL_PENDING'
SUBSCRIPTION_APPROVED = 'APPROVED'
SUBSCRIPTION_ACTIVE = 'ACTIVE'
SUBSCRIPTION_SUSPENDED = 'SUSPENDED'
SUBSCRIPTION_CANCELED = 'CANCELLED'
SUBSCRIPTION_EXPIRED = 'EXPIRED'

SUBSCRIPTION_CHOICES = (
    (SUBSCRIPTION_APPROVAL_PENDING, _('Approval pending')),
    (SUBSCRIPTION_APPROVED, _('Approved')),
    (SUBSCRIPTION_ACTIVE, _('Active')),
    (SUBSCRIPTION_SUSPENDED, _('Suspended')),
    (SUBSCRIPTION_CANCELED, _('Canceled')),
    (SUBSCRIPTION_EXPIRED, _('Expired')),
)

# ткуда брать адрес доставки
SUBSCRIPTION_SHIPPING_CUSTOMER = 'GET_FROM_FILE'
SUBSCRIPTION_SHIPPING_ABSENCE = 'NO_SHIPPING'
SUBSCRIPTION_SHIPPING_MERCH = 'SET_PROVIDED_ADDRESS'

# сразу подписывать или проверять
SUBSCRIPTION_USER_ACTION_CONTINUE = 'CONTINUE'
SUBSCRIPTION_USER_ACTION_SUBSCRIBE_NOW = 'SUBSCRIBE_NOW'

# способ оплаты
SUBSCRIPTION_PAYER_PAYPAL = 'PAYPAL'
SUBSCRIPTION_PAYER_CC = 'PAYPAL_CREDIT'

# способ принятия платежей
SUBSCRIPTION_PAYEE_UNRESTRICTED = 'UNRESTRICTED'
SUBSCRIPTION_PAYEE_IMMEDIATE = 'IMMEDIATE_PAYMENT_REQUIRED'



# ================
# Product
# ================

PRODUCT_TYPE_PHYSICAL = 'PHYSICAL'
PRODUCT_TYPE_DIGITAL = 'DIGITAL'
PRODUCT_TYPE_SERVICE = 'SERVICE'
PRODUCT_TYPE_CHOICES = (
    (PRODUCT_TYPE_PHYSICAL, _('Physical')),
    (PRODUCT_TYPE_DIGITAL, _('Digital')),
    (PRODUCT_TYPE_SERVICE, _('Service')),
)