from django.dispatch import Signal


paypal_payment = Signal(providing_args=[
    'request', 'code', 'reason', 'reasonText',
    'invoice', 'amount', 'description'
])

paypal_reccuring_transaction = Signal(providing_args=[
    'request', 'subscription_id', 'pay_number',
    'code', 'reason', 'reasonText',
])

paypal_subscription = Signal(providing_args=[
    'request', 'subscription_id', 'status'
])
