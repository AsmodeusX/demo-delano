from django.utils.translation import ugettext_lazy as _


BUSINESS_LIFE_STAGE = (
    ('SU', _('Start up')),
    ('TN', _('Transition')),
    ('MT', _('Maturity')),
    ('EN', _('Expansion')),
    ('GT', _('Growth')),
)

INDUSTRY = (
    ('AI', _('Agricultural industry')),
    ('AR', _('Arms industry')),
    ('AU', _('Automotive industry')),
    ('BC', _('Broadcasting Industry')),
    ('CH', _('Chemical industry')),
    ('CL', _('Clothing & apparel industry')),
    ('CT', _('Computer industry')),
    ('CN', _('Construction industry')),
    ('DR', _('Direct selling industry')),
    ('DB', _('Distribution industry')),
    ('ED', _('Education industry')),
    ('EL', _('Electronics industry')),
    ('EN', _('Entertainment industry')),
    ('FM', _('Film industry')),
    ('FS', _('Financial services industry')),
    ('FN', _('Fishing industry')),
    ('FD', _('Food industry')),
    ('HC', _('Health care industry')),
    ('HS', _('Hospitality industry')),
    ('II', _('Information industry')),
    ('MA', _('Manufacturing')),
    ('MM', _('Mass media industry')),
    ('MS', _('Music industry')),
    ('NM', _('News media industry')),
    ('OP', _('Other products industry')),
    ('OS', _('Other services industry')),
    ('PU', _('Publishing industry')),
    ('SF', _('Software industry')),
    ('TL', _('Telecommunications industry')),
    ('TR', _('Transportation industry')),
    ('UU', _('Unknown/unsure')),
)