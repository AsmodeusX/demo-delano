from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Config(AppConfig):
    name = 'packages'
    verbose_name = _('Packages')

    def ready(self):
        from libs.js_storage import JS_STORAGE
        from django.shortcuts import resolve_url

        JS_STORAGE.update({
            'ajax_package': resolve_url('packages:ajax_package'),
        })