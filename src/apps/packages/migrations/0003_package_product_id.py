# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('packages', '0002_time_package'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='product_id',
            field=models.CharField(max_length=255, verbose_name='paypal product id', blank=True),
        ),
    ]
