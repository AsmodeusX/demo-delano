# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields
import libs.stdimage.fields
import libs.sprite_image.fields
import libs.storages.media_storage
import libs.valute_field.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Option',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='name')),
                ('view', models.IntegerField(choices=[(1, 'Default'), (2, 'Gold'), (3, 'Platinum')], default=1, verbose_name='view option')),
                ('icon', libs.sprite_image.fields.SpriteImageField(choices=[('phone', (-138, -111)), ('consultation', (-230, -111)), ('plan', (-184, -111)), ('video', (0, -111)), ('chat', (-46, -111)), ('message', (-92, -111))], background='#333', verbose_name='icon', size=(46, 46), default='phone', sprite='img/portal_img/Sprite_B.svg')),
                ('url', models.URLField(blank=True, verbose_name='url')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email')),
                ('type_option', models.PositiveSmallIntegerField(choices=[(1, 'Popup'), (2, 'URL'), (3, 'E-mail')], default=1, verbose_name='type')),
                ('popup', models.CharField(choices=[('phone', 'Phone Call'), ('video', 'Video Call'), ('consultation', 'Consultation'), ('plan', 'Free Plan')], default='phone', blank=True, max_length=64, verbose_name='type popup')),
                ('sort_order', models.PositiveIntegerField(default=0, verbose_name='order')),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name_plural': 'options',
                'verbose_name': 'option',
            },
        ),
        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='name')),
                ('time', models.PositiveSmallIntegerField(default=20, verbose_name='number of hours')),
                ('price', libs.valute_field.fields.ValuteField(verbose_name='price')),
                ('popular', models.BooleanField(default=False, verbose_name='Popular')),
                ('description', ckeditor.fields.CKEditorField(blank=True, verbose_name='description')),
                ('active', models.BooleanField(default=True, verbose_name='active')),
                ('img', libs.stdimage.fields.StdImageField(variations={'admin': {'size': (320, 240)}, 'normal': {'size': (760, 758)}}, storage=libs.storages.media_storage.MediaStorage('main'), aspects=(), verbose_name='Package Image', upload_to='', blank=True)),
                ('sort_order', models.PositiveIntegerField(default=0, verbose_name='order')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
                ('options', models.ManyToManyField(help_text='select options for a package', to='packages.Option', verbose_name='options')),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name_plural': 'packages',
                'verbose_name': 'package',
            },
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('months', models.PositiveSmallIntegerField(verbose_name='count months')),
                ('price', libs.valute_field.fields.ValuteField(verbose_name='price')),
                ('paypal_plan_id', models.CharField(blank=True, max_length=128, verbose_name='paypal plan id')),
                ('package', models.ForeignKey(to='packages.Package', verbose_name='package')),
            ],
            options={
                'ordering': ('package', 'months'),
                'verbose_name_plural': 'plans',
                'verbose_name': 'plan',
            },
        ),
        migrations.CreateModel(
            name='Time',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('number', models.PositiveSmallIntegerField(verbose_name='number of hours')),
                ('price', libs.valute_field.fields.ValuteField(verbose_name='price')),
                ('active', models.BooleanField(default=True, verbose_name='active')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'ordering': ('number',),
                'verbose_name_plural': 'time',
                'verbose_name': 'time',
            },
        ),
    ]
