# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('packages', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='time',
            name='package',
            field=models.ForeignKey(to='packages.Package', related_name='times_package', verbose_name='package', null=True, blank=True),
        ),
    ]
