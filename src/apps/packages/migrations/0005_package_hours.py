# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('packages', '0004_auto_20191104_0600'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='hours',
            field=models.PositiveSmallIntegerField(verbose_name='hours for answer', default=2),
        ),
    ]
