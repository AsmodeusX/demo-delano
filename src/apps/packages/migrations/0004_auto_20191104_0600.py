# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import packages.models


class Migration(migrations.Migration):

    dependencies = [
        ('packages', '0003_package_product_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plan',
            name='months',
            field=packages.models.IntegerField(verbose_name='count months'),
        ),
    ]
