from django.views.generic.base import View
from django.shortcuts import resolve_url
from clients.models import Client
from libs.views_ajax import AjaxViewMixin


class PackageView(AjaxViewMixin, View):
    def get(self, request):
        client = Client.objects.get(user_id=request.user.id)
        package = client.current_package()

        return self.json_response({
            'popup': self.render_to_string('packages/popups/package.html', {
                'package': package,
                'link': resolve_url('index') + '#prices'
            })
        })