from django.db import models
from django.utils.translation import ugettext_lazy as _
from . import options
from ckeditor.fields import CKEditorField
from libs.sprite_image.fields import SpriteImageField
from users.models import CustomUser
from libs.valute_field.fields import ValuteField
from libs.storages.media_storage import MediaStorage
from libs.stdimage.fields import StdImageField
from django.utils import timezone
from paypal import api as paypal_api
from paypal import utils
import logging
import decimal

logger = logging.getLogger('django.paypal')


class IntegerField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value': self.max_value}
        defaults.update(kwargs)
        return super(IntegerField, self).formfield(**defaults)


def get_localtime(ts):
    ts_dt_utc = timezone.make_aware(timezone.datetime.strptime(ts, "%Y-%m-%dT%H:%M:%S.%fZ"), timezone=timezone.utc)
    return timezone.localtime(ts_dt_utc)


class Option(models.Model):
    name = models.CharField(_('name'), max_length=64)
    view = models.IntegerField(_('view option'), choices=options.VIEW_OPTIONS, default=options.VIEW_DEFAULT)

    icon = SpriteImageField(_('icon'),
                            sprite='img/portal_img/Sprite_B.svg',
                            size=(46, 46),
                            choices=options.ICON_OPTIONS,
                            default=options.OPTION_PHONE,
                            background='#333',
                            )
    url = models.URLField(_('url'), blank=True)
    email = models.EmailField(_('email'), blank=True)
    type_option = models.PositiveSmallIntegerField(_('type'), choices=options.TYPE_OPTIONS, default=options.TYPE_POPUP)
    popup = models.CharField(_('type popup'), choices=options.POPUP_OPTIONS, default=options.OPTION_PHONE, max_length=64, blank=True )
    sort_order = models.PositiveIntegerField(_('order'), default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('option')
        verbose_name_plural = _('options')
        ordering = ('sort_order', )


class Package(models.Model):
    name = models.CharField(_('name'), max_length=64)
    time = models.PositiveSmallIntegerField(_('number of hours'), default=20, )
    hours = models.PositiveSmallIntegerField(_('hours for answer'), default=2)
    price = ValuteField(_('price'), )
    popular = models.BooleanField(_('Popular'), default=False)

    description = CKEditorField(_('description'), blank=True)
    options = models.ManyToManyField(Option, verbose_name=_('options'), help_text=_('select options for a package'))
    product_id = models.CharField(_('paypal product id'), max_length=255, blank=True)
    active = models.BooleanField(_('active'), default=True, )
    img = StdImageField(
        _('Package Image'),
        storage=MediaStorage('main'),
        admin_variation='admin',
        blank=True,
        variations=dict(
            normal=dict(
                size=(760, 758),
            ),
            admin=dict(
                size=(320, 240),
            ),
        ),
    )
    sort_order = models.PositiveIntegerField(_('order'), default=0)
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('package')
        verbose_name_plural = _('packages')
        ordering = ('sort_order', )

    def __str__(self):
        return self.name

    def full_name(self):
        return _('{} Package').format(self.name)

    def price_month(self):
        return '{}/mo'.format(self.price)

    def save(self, *args, **kwargs):
        is_add = self.pk

        if is_add is None:
            paypal_api.define_config()

            pr = paypal_api.define_product(
                name=self.name,
                description=self.description,
            )

            if pr.create():
                self.product_id = pr.id
            else:
                logger.error('Paypal create product error: %s' % pr.error)
        else:

            original = self.__class__.objects.only(
                'description', 'product_id',
            ).get(pk=self.pk)

            update_attributes = []

            if not original.description and self.description:
                update_attributes.append({
                    'op': 'add',
                    'path': '/description',
                    'value': self.description
                })
            elif original.description and not self.description:
                update_attributes.append({
                    'op': 'remove',
                    'path': '/description'
                })
            elif original.description != self.description:
                update_attributes.append({
                    'op': 'replace',
                    'path': '/description',
                    'value': self.description
                })

            if update_attributes:
                paypal_api.define_config()
                pr = paypal_api.get_product(product_id=self.product_id)

                if not paypal_api.update_product(product=pr, attributes=update_attributes):
                    logger.error('Paypal update product ({id}) error: {err}'.format(
                        id=pr.id,
                        err=pr.error
                    ))

        super().save(*args, **kwargs)


class Time(models.Model):
    package = models.ForeignKey(Package, verbose_name=_('package'), related_name='times_package', blank=True, null=True)
    number = models.PositiveSmallIntegerField(_('number of hours'), )
    price = ValuteField(_('price'), )

    active = models.BooleanField(_('active'), default=True, )

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('time')
        verbose_name_plural = _('time')
        ordering = ('number', )

    def __str__(self):
        return '+ {} hours'.format(self.number)

    def paypal_fee_price(self):
        return round(float(str(self.price.as_decimal() + (self.price.as_decimal() * decimal.Decimal('0.03') + decimal.Decimal('0.3')))), 2)

    def detail_price(self):
        return '{}(with fee ${})'.format(self.price, self.paypal_fee_price())


class Plan(models.Model):
    months = IntegerField(_('count months'), min_value=1, max_value=12)
    price = ValuteField(_('price'), )
    package = models.ForeignKey(Package, verbose_name=_('package'), )
    paypal_plan_id = models.CharField(_('paypal plan id'), max_length=128, blank=True)

    class Meta:
        verbose_name = _('plan')
        verbose_name_plural = _('plans')
        ordering = ('package', 'months')

    def __str__(self):
        return '{}/month for {} month subscription on package {}'.format(self.price, self.months, self.package)

    def save(self, *args, **kwargs):

        billing_plans = Plan.objects.filter(
            price=self.price,
            months=self.months,
            package=self.package,
        )

        if not billing_plans.exists():
            paypal_api.define_config()

            billing_plan = paypal_api.define_plan(
                product_id=self.package.product_id,
                name=_('Delano portal plan'),
                description=_('Plan for subscription'),
                regular_amount=self.price.as_string(),
                regular_frequency_interval=1,
                regular_frequency='MONTH',
                regular_cycles=self.months,
                status='CREATED',
            )

            if billing_plan.create():
                if billing_plan.activate():
                    self.paypal_plan_id = billing_plan.id
                else:
                    logger.error('Activate billing plan{id} error: {err}'.format(
                        id=billing_plan.id,
                        err=billing_plan.error
                    ))
            else:
                logger.error('Created billing plan{id} error: {err}'.format(
                    id=billing_plan.id,
                    err=billing_plan.error
                ))
        super().save(*args, **kwargs)
