(function ($) {

    $(document).ready(function () {

        $('.current-package').on('click', function () {
            $.ajax({
                url: window.js_storage.ajax_package,
                type: 'GET',
                dataType: 'json',

                success: function(response) {
                    if (response.popup) {
                        $.popup({
                            classes: 'package-popup',
                            content: response.popup
                        }).show();
                    }
                },
                error: $.parseError(function() {
                    alert(window.DEFAULT_AJAX_ERROR);
                })
            });
        })
    });
})(jQuery);