# from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from suit.admin import SortableModelAdmin
from project.admin import ModelAdminMixin, ModelAdminInlineMixin
from .models import Package, Time, Option, Plan


# class PlanFormSet(forms.models.BaseInlineFormSet):
#     model = Plan


class PlanAdmin(ModelAdminInlineMixin, admin.StackedInline):
    model = Plan
    extra = 0
    suit_classes = 'suit-tab suit-tab-plans'
    # formset = PlanFormSet
    readonly_fields = ('paypal_plan_id', )


class TimeAdmin(ModelAdminInlineMixin, admin.StackedInline):
    model = Time
    extra = 0
    min_num = 0
    suit_classes = 'suit-tab suit-tab-time'


@admin.register(Option)
class PackageAdmin(ModelAdminMixin, SortableModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name', 'view', 'icon', 'type_option', 'popup', 'url', 'email',
            ),
        }),
    )
    list_display = ('name', 'view', )
    sortable = 'sort_order'
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(Package)
class PackageAdmin(ModelAdminMixin, SortableModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name', 'time', 'hours', 'price', 'description', 'active', 'popular', 'img', 'options', 'product_id',
            ),
        }),
    )
    inlines = (PlanAdmin, TimeAdmin, )
    list_display = ('name', 'active', 'popular')
    sortable = 'sort_order'
    readonly_fields = ('product_id', )
    suit_form_tabs = (
        ('general', _('General')),
        ('plans', _('Plans')),
        ('time', _('Additional Time')),
    )


@admin.register(Plan)
class PlanAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'paypal_plan_id', 'months', 'price', 'package',
            ),
        }),
    )
    list_display = ('paypal_plan_id', 'months', 'price', )
    suit_form_tabs = (
        ('general', _('General')),
    )
    readonly_fields = ('paypal_plan_id', )
