from django.utils.translation import ugettext_lazy as _


VIEW_DEFAULT = 1
VIEW_GOLD = 2
VIEW_PLATINUM = 3

VIEW_OPTIONS = (
    (VIEW_DEFAULT, _('Default')),
    (VIEW_GOLD, _('Gold')),
    (VIEW_PLATINUM, _('Platinum')),
)


TYPE_POPUP = 1
TYPE_URL = 2
TYPE_EMAIL = 3

TYPE_OPTIONS = (
    (TYPE_POPUP, _('Popup')),
    (TYPE_URL, _('URL')),
    (TYPE_EMAIL, _('E-mail')),
)


OPTION_PHONE = 'phone'
OPTION_CONSULTATION = 'consultation'
OPTION_PLAN = 'plan'
OPTION_VIDEO = 'video'
OPTION_CHAT = 'chat'
OPTION_MESSAGE = 'message'

ICON_OPTIONS = (
    (OPTION_PHONE, (-138, -111)),
    (OPTION_CONSULTATION, (-230, -111)),
    (OPTION_PLAN, (-184, -111)),
    (OPTION_VIDEO, (-0, -111)),
    (OPTION_CHAT, (-46, -111)),
    (OPTION_MESSAGE, (-92, -111)),
)

POPUP_OPTIONS = (
    (OPTION_PHONE, _('Phone Call')),
    (OPTION_VIDEO, _('Video Call')),
    (OPTION_CONSULTATION, _('Consultation')),
    (OPTION_PLAN, _('Free Plan')),
)