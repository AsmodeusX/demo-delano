from django.conf.urls import url
from . import views_ajax

urlpatterns = [
    url(r'^ajax/package/', views_ajax.PackageView.as_view(), name='ajax_package'),
]