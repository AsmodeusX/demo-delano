from django.db import models
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from packages.models import Package, Time, Plan
from clients.models import Client
from libs.valute_field.fields import ValuteField
from django.utils.timezone import now
from django.utils import timezone
from . import options
from dateutil.relativedelta import relativedelta
from packages import options as packages_options


class Subscription(models.Model):
    client = models.ForeignKey(Client, verbose_name=_('client'), related_name='subscription_client', blank=True, null=True)
    status = models.CharField(_('status'), max_length=64, default=options.AGREEMENT_PENDING, choices=options.AGREEMENT_CHOICES)
    plan = models.ForeignKey(Plan, verbose_name=_('plan'), blank=True, null=True)
    subscription_id = models.CharField(_('subscription id'), max_length=128, blank=True)
    start_date = models.DateTimeField(_('start date'), default=now, )
    created = models.DateTimeField(_('create date'), auto_now=True)

    class Meta:
        verbose_name = _('subscription')
        verbose_name_plural = _('subscriptions')

    def __str__(self):
        return _('Subscription #{}').format(self.id)

    def active(self):
        return Purchase.objects.filter(purchase_id=self.id, payment_complete=True)

    def plan_active(self):
        options_list = self.plan.package.options.filter(popup=packages_options.OPTION_PLAN)
        stat = options_list.exists()
        queries = self.client.query_client.all()

        if stat:
            queries = queries.filter(subscription_id=self.id, option__popup=packages_options.OPTION_PLAN)
            for query in queries:

                if query.time_query.plan:
                    return False
        else:
            stat = True

        return stat

    def consultation_active(self):
        options_list = self.plan.package.options.filter(popup=packages_options.OPTION_CONSULTATION)
        stat = options_list.exists()
        queries = self.client.query_client.all()

        if stat:
            queries = queries.filter(subscription_id=self.id, option__popup=packages_options.OPTION_CONSULTATION)
            for query in queries:
                if query.time_query.consultation:
                    return False
        else:
            stat = True

        return stat

    def get_cur_end_period(self):
        n = 1
        start_date = self.start_date
        m = self.plan.months + 1
        t = timezone.now()

        for x in list(range(n, m)):
            current_period_end = start_date + relativedelta(months=+x)

            if start_date <= t < current_period_end:
                return current_period_end
        return None

    def aggregate_hours(self):
        t = timezone.now()
        start_date = self.start_date
        end_date = start_date + relativedelta(month=+self.plan.months)
        cur_end_period = self.get_cur_end_period()
        cur_start_period = cur_end_period - relativedelta(month=+1)

        additional_hours = 0
        default_hours = self.plan.package.time

        if start_date > t <= end_date:
            return 0

        if cur_start_period <= t < cur_end_period:
            additional_times = AdditionalHours.objects.filter(subscription_id=self.id)
            at_list = []

            for additional_time in additional_times:
                if cur_start_period <= additional_time.created < cur_end_period:
                    at_list.append(additional_time.time.number)

            for at in at_list:
                additional_hours += int(at)

        return default_hours + additional_hours

    def west_hours(self):
        hours = 0
        current_month_end = self.get_cur_end_period()
        current_month_start = current_month_end + relativedelta(months=-1)

        for query in self.query_subscription.all():

            if current_month_end.date() > query.date >= current_month_start.date():
                if query.wt():
                    hours += int(query.wt().hours) or 0

        if hours >= 0:
            return hours

        return 0

    def summary_hours(self):
        ah = self.aggregate_hours()
        wh = self.west_hours()

        difference = ah - wh

        if difference < 0:
            difference = 0

        return difference

    def subscriptions(self):
        return Subscription.objects.filter(client_id=self.id)

    def summary_days(self):
        t = timezone.now()

        current_month_end = self.get_cur_end_period()
        days = (current_month_end - t).days
        if days >= 0:
            return (current_month_end - t).days

        return 0


class AdditionalHours(models.Model):
    subscription = models.ForeignKey(Subscription, verbose_name=_('subscription'))
    time = models.ForeignKey(Time, verbose_name=_('time'), )
    created = models.DateTimeField(_('create date'), auto_now=True)
    payment_id = models.CharField(_('payment id'), max_length=128, blank=True)
    status = models.CharField(_('status'), max_length=128, blank=True)

    class Meta:
        verbose_name = _('additional hours')
        verbose_name_plural = _('additional hours')
        ordering = ('-created', )


class Purchase(models.Model):
    subscription = models.OneToOneField(Subscription,
                                        verbose_name=_('subscription'),
                                        blank=True,
                                        null=True,
                                        related_name='purchase_subscription')
    additional_hours = models.OneToOneField(AdditionalHours,
                                            verbose_name=_('additional hours'),
                                            blank=True,
                                            null=True,
                                            related_name='purchase_hours')
    price = ValuteField(_('price'), )
    name = models.CharField(_('name'), max_length=128, default='')
    email = models.CharField(_('email'), max_length=128, default='')
    error_message = models.TextField(_('error message'), blank=True, )

    class Meta:
        verbose_name = _('purchase', )
        verbose_name_plural = _('purchases')
        ordering = ('-id', )

    def client(self):
        if self.additional_hours:
            return self.additional_hours.subscription.client
        elif self.subscription:
            return self.subscription.client
        else:
            return None


class Transaction(models.Model):
    purchase = models.ForeignKey(Purchase,
                                        verbose_name=_('purchase'),
                                        related_name='transactions_purchase')

    price = ValuteField(_('email'), )
    date = models.DateTimeField(_('date'), auto_now=True)

    class Meta:
        verbose_name = _('transaction', )
        verbose_name_plural = _('transactions')

    def name_email(self):
        if self.purchase.name and self.purchase.email:
            return '{} / {}'.format(self.purchase.name, self.purchase.email)

        return ''
