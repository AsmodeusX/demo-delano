from django.views.generic.base import View
from libs.views_ajax import AjaxViewMixin
from .forms import SubscriptionForm, PolicyForm
from .models import Plan, Time, AdditionalHours, Purchase, Subscription, Package
from clients.models import Client
from django.shortcuts import resolve_url, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from paypal import api as paypal_api
from django.utils import timezone
from paypal.api import define_payment
from django.contrib.auth import authenticate, login as auth_login
from users.views import create_user
from dateutil.relativedelta import relativedelta
from clients.views import create_client, get_client
from subscriptions.options import AGREEMENT_CANCELED, AGREEMENT_EXPIRED, AGREEMENT_SUSPENDED

import logging
import datetime



logger = logging.getLogger('django.paypal')


class SubscriptionBuyView(AjaxViewMixin, View):
    def get(self, request):

        package = request.GET.get('package')

        if not package:
            return self.json_error({
                'error': 'Package required'
            })

        pkg = get_object_or_404(Package, id=package)

        try:
            Client.objects.get(user_id=request.user.id)
            form = PolicyForm(package)

            return self.json_response({
                'popup': self.render_to_string('subscriptions/popups/subscription.html', {
                    'form': form,
                    'client': True,
                    'package': pkg
                }),
            })
        except ObjectDoesNotExist:
            form = SubscriptionForm(package)

            return self.json_response({
                'popup': self.render_to_string('subscriptions/popups/subscription.html', {
                    'form': form,
                    'package': pkg
                }),
            })

    def post(self, request):
        package_id = request.POST.get('package')
        plan = Plan.objects.get(id=request.POST.get('plan_choices'))

        """Start"""
        if request.user.is_authenticated():
            form = PolicyForm(package_id, request.POST)

            if form.is_valid():
                client = get_client(request)
                if not client:
                    raise PermissionDenied

            else:
                return self.json_error({
                    'errors': form.error_dict_full,
                })

        else:
            form = SubscriptionForm(package_id, request.POST)

            if form.is_valid():
                first_name = request.POST.get('first_name')
                last_name = request.POST.get('last_name')
                email = request.POST.get('email')
                user = create_user(request, first_name=first_name, last_name=last_name, email=email)
                u = authenticate(username=user.get('user').username, password=user.get('pwd'))
                auth_login(request, u)
                client = create_client(request, user=user)
            else:

                return self.json_error({
                    'errors': form.error_dict_full,
                })

        """Finish"""
        paypal_api.define_config()
        subscription_plan = paypal_api.get_plan(plan.paypal_plan_id)
        if subscription_plan.status == 'ACTIVE':
            # Дата начала соглашения, должна быть больше текущей
            start_date = timezone.now()
            # если у нас есть планы, то учитываем дату последнего(она будет наибольшей)

            subscriptions = client.subscription_client\
                .filter(start_date__gte=start_date)\
                .exclude(status=AGREEMENT_SUSPENDED)\
                .exclude(status=AGREEMENT_EXPIRED)\
                .exclude(status=AGREEMENT_CANCELED) \
                .exclude(subscription_id='')

            if subscriptions.exists():
                last_subscription = subscriptions.last()
                start_date = last_subscription.start_date + relativedelta(months=+last_subscription.plan.months)

            # формат даты под пейпал
            start_date_now = start_date.replace(tzinfo=datetime.timezone.utc).replace(microsecond=0).isoformat()
            start_date_later = (start_date + relativedelta(minutes=+5)).replace(tzinfo=datetime.timezone.utc).replace(microsecond=0).isoformat()
            subscription = paypal_api.define_subscription(
                plan_id=plan.paypal_plan_id,
                start_date=start_date_later,
                first_name=client.user.first_name,
                last_name=client.user.last_name,
                email=client.user.email,
                return_url=request.build_absolute_uri(resolve_url('paypal:subscription_result')),
                cancel_url=request.build_absolute_uri(resolve_url('index')),
                # amount=plan.price.as_string()
            )

            if subscription.create():
                s = Subscription.objects.create(
                    client=client,
                    plan_id=plan.id,
                    start_date=start_date_now,
                )
                Purchase(
                    price=plan.price,
                    subscription_id=s.id
                ).save()
                return self.json_response({
                    'paypal': subscription.get_approve_url()
                })
            else:
                logger.error('Created billing agreement{id} error: {err}'.format(
                    id=subscription.id,
                    err=subscription.error
                ))
                return self.json_error({
                    'popup_close': True,
                    'error': subscription.error
                })

        else:
            return self.json_error({
                'popup_close': True,
                'error': 'Billing plan have status another active'
            })


class HoursBuyView(AjaxViewMixin, View):
    def get(self, request):
        try:
            user = request.user
            client = Client.objects.get(user_id=user.id)
        except ObjectDoesNotExist:
            return self.json_error({
                'error': "Client doesn't exist"
            })

        hours = Time.objects.filter(active=True, package_id=client.current_package().id)

        if not hours:
            return self.json_error({
                'error': 'Additional Hours Not Found'
            })

        return self.json_response({
            'popup': self.render_to_string('subscriptions/popups/hours.html', {
                'title': 'MyBizExpert: {} Package'.format(client.current_package()),
                'additional_hours': hours,
            }),
        })

    def post(self, request):
        try:
            hours = Time.objects.get(id=request.POST.get('hours'))
        except ObjectDoesNotExist:
            return self.json_error({
                'error': 'Unknown Hours Id'
            })

        try:
            client = Client.objects.get(user_id=request.user.id)
        except ObjectDoesNotExist:
            return self.json_error({
                'error': 'Unknown Client Id'
            })

        additional_hours = AdditionalHours(
            subscription_id=client.current_subscription().id,
            time_id=hours.id
        )
        additional_hours.save()

        paypal_api.define_config()

        purchase_obj = Purchase(
            price=hours.price.as_decimal(),
            additional_hours_id=additional_hours.id,
        )
        purchase_obj.save()

        payment = define_payment(
            request,
            amount=hours.price.as_string(),
            description='Buy {} additional hours for {} package on Delano Portal'.format(
                hours.number, client.current_package()
            ),
            payment_id=purchase_obj.id
        )


        if payment.create():

            for link in payment.links:
                if link.rel == "approval_url":

                    return self.json_response({
                        'link': link.href
                    })
        else:
            logging.error(payment.error)

            return self.json_response({
                'error': payment.error
            })
