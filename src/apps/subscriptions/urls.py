from django.conf.urls import url
from . import views_ajax


urlpatterns = [
    url(r'^ajax/buy/subscription/$', views_ajax.SubscriptionBuyView.as_view(), name='ajax_buy_subscription'),
    url(r'^ajax/buy/hours/$', views_ajax.HoursBuyView.as_view(), name='ajax_buy_hours'),
]
