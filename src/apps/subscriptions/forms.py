from django import forms
from libs.form_helper.forms import FormHelperMixin
from .models import Plan, Subscription
from django.utils.translation import ugettext_lazy as _
from libs.widgets import PhoneWidget
from config import options as config_options


class SubscriptionForm(FormHelperMixin, forms.ModelForm):
    default_field_template = 'form_helper/unlabeled_field.html'
    field_templates = {
        'plan_choices': 'form_helper/choice_field.html',
    }

    agree = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={
            'required': True,
        })
    )

    first_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('First Name'),
            }),
        error_messages={
            'required': _('Please enter your first name'),
            'max_length': _('First Name should not be longer than %(limit_value)d characters'),
        }
    )

    last_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('First Name'),
            }),
        error_messages={
            'required': _('Please enter your last name'),
            'max_length': _('First Name should not be longer than %(limit_value)d characters'),
        }
    )

    address = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('Address'),
            }),
        error_messages={
            'required': _('Please enter your address'),
            'max_length': _('Last Name should not be longer than %(limit_value)d characters'),
        }
    )

    company = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('Company Name'),
            }),
        error_messages={
            'required': _('Please enter your company name'),
            'max_length': _('Company Name should not be longer than %(limit_value)d characters'),
        }
    )

    position = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('Position'),
            }),
        error_messages={
            'required': _('Please enter your position'),
            'max_length': _('Position should not be longer than %(limit_value)d characters'),
        }
    )

    phone = forms.CharField(
        widget=PhoneWidget(
            attrs={
                'placeholder': _('Phone'),
            }),
        error_messages={
            'required': _('Please enter your phone'),
            'max_length': _('phone should not be longer than %(limit_value)d characters'),
        }
    )

    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                'placeholder': _('E-mail'),
            }),
        error_messages={
            'required': _('Please enter your e-mail'),
            'max_length': _('E-mail should not be longer than %(limit_value)d characters'),
        }
    )

    stage = forms.CharField(widget=forms.Select(choices=config_options.BUSINESS_LIFE_STAGE))
    industry = forms.CharField(widget=forms.Select(choices=config_options.INDUSTRY))

    def __init__(self, package=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['plan_choices'] = forms.ChoiceField(
            choices=[(o.id, o.__str__()) for o in Plan.objects.filter(package_id=package)],
            widget=forms.RadioSelect(),
        )

    class Meta:
        model = Subscription
        exclude = ('status', 'start_date', )

    def clean_first_name(self):
        if 'first_name' in self.cleaned_data:
            first_name = self.cleaned_data.get('first_name')

            if not first_name:
                self.add_field_error('first_name', 'required')

    def clean_last_name(self):
        if 'last_name' in self.cleaned_data:
            last_name = self.cleaned_data.get('last_name')

            if not last_name:
                self.add_field_error('last_name', 'required')

    def clean_address(self):
        if 'address' in self.cleaned_data:
            address = self.cleaned_data.get('address')

            if not address:
                self.add_field_error('address', 'required')

    def clean_phone(self):
        if 'phone' in self.cleaned_data:
            phone = self.cleaned_data.get('phone')

            if not phone:
                self.add_field_error('phone', 'required')

    def clean_email(self):
        if 'email' in self.cleaned_data:
            email = self.cleaned_data.get('email')

            if not email:
                self.add_field_error('email', 'required')


class PolicyForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/unlabeled_field.html'

    field_templates = {
        'plan_choices': 'form_helper/choice_field.html',
    }

    agree = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={
            'required': True,
        })
    )

    def __init__(self, package=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['plan_choices'] = forms.ChoiceField(
            choices=[(o.id, o.__str__()) for o in Plan.objects.filter(package_id=package)],
            widget=forms.RadioSelect(),
        )
