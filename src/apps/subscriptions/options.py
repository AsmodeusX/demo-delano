from django.utils.translation import ugettext_lazy as _


VIEW_TIME = 1
VIEW_PACKAGE = 2

VIEW_OPTIONS = (
    (VIEW_TIME, _('Hours')),
    (VIEW_PACKAGE, _('Package')),
)


# Billing Agreement Status
AGREEMENT_PENDING = 'PENDING'
AGREEMENT_ACTIVE = 'ACTIVE'
AGREEMENT_SUSPENDED = 'SUSPENDED'
AGREEMENT_CANCELED = 'CANCELED'
AGREEMENT_EXPIRED = 'EXPIRED'


AGREEMENT_CHOICES = (
    (AGREEMENT_PENDING, _('Pending')),
    (AGREEMENT_ACTIVE, _('Active')),
    (AGREEMENT_SUSPENDED, _('Suspended')),
    (AGREEMENT_CANCELED, _('Canceled')),
    (AGREEMENT_EXPIRED, _('Expired'))
)
