import logging
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from project.admin import ModelAdminMixin, ModelAdminInlineMixin
from django.utils import timezone
from .models import Purchase, Subscription, AdditionalHours
from django.core.exceptions import ObjectDoesNotExist


logger = logging.getLogger('django.paypal')


def get_localtime(ts):
    ts_dt_utc = timezone.make_aware(timezone.datetime.strptime(ts, "%Y-%m-%dT%H:%M:%S.%fZ"), timezone=timezone.utc)
    return timezone.localtime(ts_dt_utc)


class AdditionalHoursInline(ModelAdminInlineMixin, admin.StackedInline):
    model = AdditionalHours
    extra = 0
    suit_classes = 'suit-tab suit-tab-general'


class PurchaseInline(ModelAdminInlineMixin, admin.StackedInline):
    model = Purchase
    max_num = 1
    min_num = 0
    extra = 0
    suit_classes = 'suit-tab suit-tab-general'
    readonly_fields = ('price', 'error_message', )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Subscription)
class SubscriptionAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'plan', 'client',
            ),
        }),
    )
    list_display = ('__str__', 'client', 'plan', )
    list_per_page = 20
    search_fields = ('client', )
    inlines = (PurchaseInline, AdditionalHoursInline, )
    suit_form_tabs = (
        ('general', _('General')),
    )
    readonly_fields = ('plan', 'client', )

    @staticmethod
    def price(obj):
        try:
            return obj.payment.price
        except ObjectDoesNotExist:
            return ''

    @staticmethod
    def status(obj):
        try:
            return obj.payment.get_status_display()
        except ObjectDoesNotExist:
            return _('Purchase not received')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False