(function ($) {
    var package_id, $subscription_popup;
    /*
     Отправка AJAX-формы покупки из попапа
     */
    $(document).on('submit', '#ajax-buy-subscription-form', function() {
        var $form = $('#ajax-buy-subscription-form');

        var data = $form.serializeArray();
        data.push({
            'name': 'package',
            'value': package_id
        });

        if ($form.hasClass('sending')) {
            return false;
        }

        $.ajax({
            url: window.js_storage.ajax_buy_subscription,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $.popup.showPreloader();
                $form.addClass('sending');
                $form.find('.invalid').removeClass('invalid');
            },
            success: function(response) {
                if (response.paypal) {
                    location.href = response.paypal
                }
            },
            error: $.parseError(function(response) {
                if (response && response.errors) {
                    // ошибки формы
                    response.errors.forEach(function(record) {
                        var $field = $form.find('.' + record.fullname);
                        if ($field.length) {
                            $field.addClass(record.class);
                        }
                    });
                } else {
                    alert(window.DEFAULT_AJAX_ERROR);
                }
            }),
            complete: function() {
                $form.removeClass('sending');
                $.popup.hidePreloader();
            }
        });

        return false;
    });

    /*
     Вывод AJAX-формы покупки пакета в попап
     */
    $('.open-buy-subscription-popup').on('click', function() {
        package_id = $(this).data('package');

        $.ajax({
            url: window.js_storage.ajax_buy_subscription,
            type: 'GET',
            data: {
                'package': package_id
            },
            dataType: 'json',
            success: function(response) {
                if (response.popup) {
                    // сообщение о успешной отправке
                    $subscription_popup = $.popup({
                        content: response.popup
                    }).on('after_show', function () {
                        initSelect();
                        $('.field-plan_choices input').eq(0).trigger('click');
                    }).on('after_show', function () {
                        var $btn = $('#buy-subscription');
                        $btn.prop('disabled', true);

                        $('#id_agree').change(function() {

                            $btn.prop('disabled', function(i, val) {
                                return !val;
                            })
                        });
                    }).show();
                }
            },
            error: $.parseError(function() {
                alert(window.DEFAULT_AJAX_ERROR);
            })
        });

        return false;
    });

    $('.open-buy-hours-popup').on('click', function () {
        $.ajax({
            url: window.js_storage.ajax_buy_hours,
            type: 'GET',
            dataType: 'json',

            success: function(response) {
                if (response.popup) {
                    $.popup({
                        classes: 'buy-hours-popup',
                        content: response.popup
                    }).show();
                }
            },
            error: $.parseError(function() {
                alert(window.DEFAULT_AJAX_ERROR);
            })
        });
    });

    $(document).on('click', '.buy-hours', function(e) {
        $.ajax({
            url: window.js_storage.ajax_buy_hours,
            type: 'POST',
            data: {
                'hours': $(e.currentTarget).parent('.hours').data('hours')
            },
            dataType: 'json',

            success: function(response) {
                if (response && response.link) {
                    window.location.href = response.link
                }
            },
            error: $.parseError(function(response) {
                if (response.popup_close) {
                    $subscription_popup.hide();
                }
                alert(window.DEFAULT_AJAX_ERROR);
            })
        });
    });

    function initSelect(){
        var $contact_form = $('#ajax-buy-subscription-form');
        $contact_form.find('select').each(function() {
            var $select = $(this),
                $control = $select.closest('.control');

            $select.selectmenu({
                appendTo: $control,
                select: function( event, ui ) {
                    // красим выбранный пункт из селекта в обычный цвет шрифта формы после выбора
                    $control.find('.ui-selectmenu-text').css({
                        'color': '#35363a',
                        'font-weight': 'bold'
                    });
                }
            });

            $control.css('position', 'relative');
        });
    }

    $(document).on('change', 'input[name="plan_choices"]', function() {
        var s = $(this).siblings('label').find('span').html(),
            str = s.split('/')[0],
            i = str.split('$')[1],
            $payBtn = $('.popup-submit');
        var fees = parseFloat(i) * 0.03 + 0.3;
        $payBtn.text('Pay ' + (parseFloat(i) + fees).toFixed(2) + ' (with fee $' + (fees).toFixed(2) + ')');
    });
})(jQuery);