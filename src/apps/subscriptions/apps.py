from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Config(AppConfig):
    name = 'subscriptions'
    verbose_name = _('Subscriptions')

    def ready(self):
        from django.shortcuts import resolve_url
        from libs.js_storage import JS_STORAGE

        JS_STORAGE.update({
            'ajax_buy_subscription': resolve_url('subscriptions:ajax_buy_subscription'),
            'ajax_buy_hours': resolve_url('subscriptions:ajax_buy_hours'),
        })
