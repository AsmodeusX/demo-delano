# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('subscriptions', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='subscription',
            old_name='paypal_agreement_id',
            new_name='subscription_id',
        ),
        migrations.AlterField(
            model_name='purchase',
            name='additional_hours',
            field=models.OneToOneField(blank=True, related_name='purchase_hours', to='subscriptions.AdditionalHours', verbose_name='additional hours', null=True),
        ),
        migrations.AlterField(
            model_name='purchase',
            name='subscription',
            field=models.OneToOneField(blank=True, related_name='purchase_subscription', to='subscriptions.Subscription', verbose_name='subscription', null=True),
        ),
    ]
