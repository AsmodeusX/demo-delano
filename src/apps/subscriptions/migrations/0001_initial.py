# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.valute_field.fields
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0001_initial'),
        ('packages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdditionalHours',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now=True, verbose_name='create date')),
                ('payment_id', models.CharField(blank=True, max_length=128, verbose_name='payment id')),
                ('status', models.CharField(blank=True, max_length=128, verbose_name='status')),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name_plural': 'additional hours',
                'verbose_name': 'additional hours',
            },
        ),
        migrations.CreateModel(
            name='Purchase',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('price', libs.valute_field.fields.ValuteField(verbose_name='price')),
                ('payment_complete', models.BooleanField(default=False, verbose_name='payment complete')),
                ('error_message', models.TextField(blank=True, verbose_name='error message')),
                ('name', models.CharField(blank=True, max_length=128, verbose_name='name client paypal')),
                ('additional_hours', models.ForeignKey(null=True, verbose_name='additional hours', to='subscriptions.AdditionalHours', related_name='purchase_hours', blank=True)),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name_plural': 'purchases',
                'verbose_name': 'purchase',
            },
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('status', models.CharField(choices=[('PENDING', 'Pending'), ('ACTIVE', 'Active'), ('SUSPENDED', 'Suspended'), ('CANCELED', 'Canceled'), ('EXPIRED', 'Expired')], default='PENDING', max_length=64, verbose_name='status')),
                ('paypal_agreement_id', models.CharField(blank=True, max_length=128, verbose_name='paypal agreement')),
                ('start_date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='start date')),
                ('created', models.DateTimeField(auto_now=True, verbose_name='create date')),
                ('client', models.ForeignKey(null=True, verbose_name='client', to='clients.Client', related_name='subscription_client', blank=True)),
                ('plan', models.ForeignKey(null=True, verbose_name='plan', to='packages.Plan', blank=True)),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name_plural': 'subscriptions',
                'verbose_name': 'subscription',
            },
        ),
        migrations.AddField(
            model_name='purchase',
            name='subscription',
            field=models.ForeignKey(null=True, verbose_name='subscription', to='subscriptions.Subscription', related_name='purchase_subscription', blank=True),
        ),
        migrations.AddField(
            model_name='additionalhours',
            name='subscription',
            field=models.ForeignKey(to='subscriptions.Subscription', verbose_name='subscription'),
        ),
        migrations.AddField(
            model_name='additionalhours',
            name='time',
            field=models.ForeignKey(to='packages.Time', verbose_name='time'),
        ),
    ]
