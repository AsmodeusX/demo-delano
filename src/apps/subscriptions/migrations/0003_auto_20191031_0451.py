# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.valute_field.fields


class Migration(migrations.Migration):

    dependencies = [
        ('subscriptions', '0002_auto_20191028_0256'),
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='price', max_length=128)),
                ('email', models.EmailField(verbose_name='email', max_length=128)),
                ('price', libs.valute_field.fields.ValuteField(verbose_name='email')),
                ('date', models.DateTimeField(verbose_name='date', auto_now=True)),
            ],
            options={
                'verbose_name': 'transaction',
                'verbose_name_plural': 'transactions',
            },
        ),
        migrations.RemoveField(
            model_name='purchase',
            name='name',
        ),
        migrations.RemoveField(
            model_name='purchase',
            name='payment_complete',
        ),
        migrations.AlterField(
            model_name='subscription',
            name='subscription_id',
            field=models.CharField(verbose_name='subscription id', blank=True, max_length=128),
        ),
        migrations.AddField(
            model_name='transaction',
            name='purchase',
            field=models.ForeignKey(verbose_name='purchase', related_name='transactions_purchase', to='subscriptions.Purchase'),
        ),
    ]
