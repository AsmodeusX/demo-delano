# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('subscriptions', '0004_auto_20191101_0203'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transaction',
            name='email',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='name',
        ),
    ]
