# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('subscriptions', '0003_auto_20191031_0451'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='subscription',
            options={'verbose_name_plural': 'subscriptions', 'verbose_name': 'subscription'},
        ),
        migrations.AddField(
            model_name='purchase',
            name='email',
            field=models.CharField(max_length=128, verbose_name='email', default=''),
        ),
        migrations.AddField(
            model_name='purchase',
            name='name',
            field=models.CharField(max_length=128, verbose_name='name', default=''),
        ),
    ]
