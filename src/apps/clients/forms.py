from django import forms
from django.utils.translation import ugettext_lazy as _
from libs.form_helper.forms import FormHelperMixin
from .models import Client
from libs.widgets import PhoneWidget
from django.contrib.auth.forms import (
    AuthenticationForm,
    SetPasswordForm as DefaultSetPasswordForm,
    PasswordChangeForm
)


class LoginForm(FormHelperMixin, AuthenticationForm):
    default_field_template = 'form_helper/unlabeled_field.html'

    username = forms.CharField(
        max_length=30,
        widget=forms.TextInput(attrs={
            'autofocus': True,
            'placeholder': _('Login')
        }),
        error_messages={
            'required': _('Please enter your login')
        }
    )
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'placeholder': _('Password')
        }),
        error_messages={
            'required': _('Please enter your password')
        }
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class ProfileForm(FormHelperMixin, forms.ModelForm):
    first_name = forms.CharField(
        label=_('First name'),
        widget=forms.TextInput(),
        error_messages={
            'required': _('Please enter your first name')
        }
    )

    last_name = forms.CharField(
        label=_('Last name'),
        widget=forms.TextInput(),
        error_messages={
            'required': _('Please enter your last name')
        }
    )

    email = forms.EmailField(
        label=_('Email'),
        widget=forms.EmailInput(),
        error_messages={
            'required': _('Please enter your email')
        }
    )

    phone = forms.CharField(
        label=_('Phone'),
        widget=forms.TextInput(),
        error_messages={
            'required': _('Please enter your phone')
        }
    )

    address = forms.CharField(
        label=_('Address'),
        widget=forms.TextInput(),
        error_messages={
            'required': _('Please enter your address')
        }
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = Client
        fields = ('first_name', 'last_name', 'email', 'phone', 'address', )

    def clean_first_name(self):
        if 'first_name' in self.cleaned_data:
            first_name = self.cleaned_data.get('first_name')

            if not first_name:
                self.add_field_error('first_name', 'required')

            return first_name

    def clean_last_name(self):
        if 'last_name' in self.cleaned_data:
            last_name = self.cleaned_data.get('last_name')

            if not last_name:
                self.add_field_error('last_name', 'required')

            return last_name

    def clean_email(self):
        if 'email' in self.cleaned_data:
            email = self.cleaned_data.get('email')

            if not email:
                self.add_field_error('email', 'required')

            return email


class ChangePasswordForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/unlabeled_field.html'
    error_messages = dict({
        'password_incorrect': _("Your old password was entered incorrectly. Please enter it again."),
    })

    old_password = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'autofocus': True,
            'placeholder': _("Old password")
        }),
        error_messages={
            'required': _('Please enter current password'),
        }
    )
    new_password = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'placeholder': _("New password")
        }),
        error_messages={
            'required': _('Please enter new password'),
        }
    )

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password'])
        if commit:
            self.user.save()
        return self.user


class RegisterForm(FormHelperMixin, forms.ModelForm):
    default_field_template = 'form_helper/unlabeled_field.html'


    agree = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={
            'required': True,
        })
    )

    first_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('First Name'),
            }),
        error_messages={
            'required': _('Please enter your first name'),
            'max_length': _('First Name should not be longer than %(limit_value)d characters'),
        }
    )

    last_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('Last Name'),
            }),
        error_messages={
            'required': _('Please enter your last name'),
            'max_length': _('Last Name should not be longer than %(limit_value)d characters'),
        }
    )

    address = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('Address'),
            }),
        error_messages={
            'required': _('Please enter your address'),
            'max_length': _('Last Name should not be longer than %(limit_value)d characters'),
        }
    )

    company = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('Company Name'),
            }),
        error_messages={
            'required': _('Please enter your company name'),
            'max_length': _('Company Name should not be longer than %(limit_value)d characters'),
        }
    )

    position = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('Position'),
            }),
        error_messages={
            'required': _('Please enter your position'),
            'max_length': _('Position should not be longer than %(limit_value)d characters'),
        }
    )

    phone = forms.CharField(
        widget=PhoneWidget(
            attrs={
                'placeholder': _('Phone'),
            }),
        error_messages={
            'required': _('Please enter your phone'),
            'max_length': _('phone should not be longer than %(limit_value)d characters'),
        }
    )

    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                'placeholder': _('E-mail'),
            }),
        error_messages={
            'required': _('Please enter your e-mail'),
            'max_length': _('E-mail should not be longer than %(limit_value)d characters'),
        }
    )

    class Meta:
        fields = '__all__'
        model = Client

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean_first_name(self):
        if 'first_name' in self.cleaned_data:
            first_name = self.cleaned_data.get('first_name')

            if not first_name:
                self.add_field_error('first_name', 'required')

    def clean_last_name(self):
        if 'last_name' in self.cleaned_data:
            last_name = self.cleaned_data.get('last_name')

            if not last_name:
                self.add_field_error('last_name', 'required')

    def clean_address(self):
        if 'last_name' in self.cleaned_data:
            last_name = self.cleaned_data.get('last_name')

            if not last_name:
                self.add_field_error('last_name', 'required')

    def clean_phone(self):
        if 'phone' in self.cleaned_data:
            phone = self.cleaned_data.get('phone')

            if not phone:
                self.add_field_error('phone', 'required')

    def clean_email(self):
        if 'email' in self.cleaned_data:
            email = self.cleaned_data.get('email')

            if not email:
                self.add_field_error('email', 'required')


class PolicyForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/unlabeled_field.html'

    field_templates = {
        'plan_choices': 'form_helper/choice_field.html',
    }

    agree = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={
            'required': True,
        })
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
