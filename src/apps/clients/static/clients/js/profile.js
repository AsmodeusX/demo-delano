(function ($) {
    /** @namespace window.js_storage.avatar_upload */
    /** @namespace window.js_storage.avatar_crop */
    /** @namespace window.js_storage.avatar_delete */

        // Обновление фото на странице
    var change_photo = function(response) {
            // Обновление кнопок
            $.loadImageDeferred(response.normal_photo).done(function() {
                $('.client-photo img').attr('src', response.normal_photo);
                initUploader();
            });
            $.loadImageDeferred(response.small_photo).done(function() {
                $('.controls-photo img').attr('src', response.small_photo);
                initUploader();
            });
        };

    // Инициализация загрузчика аватарки
    var initUploader = function() {
        return Uploader($('.photo-block'), {
            url: window.js_storage.photo_upload,
            buttonSelector: '#upload-avatar',
            multiple: false,
            resize: {
                width: 1024,
                height: 1024
            },
            max_size: '6mb',

            onFileUploaded: function(file, json_response) {
                if (json_response) {
                    change_photo(json_response);
                }
            },
            onFileUploadError: function(file, error, json_response) {
                if (json_response && json_response.message) alert(json_response.message)
            }
        });
    };

    $(document).ready(function () {
        $('#update-client-data').on('click', function (e) {
            e.preventDefault();

            var $form = $('#data-client');

            $.ajax({
                url: $form.prop('action'),
                type: 'POST',
                data: $form.serialize(),
                dataType: 'json',
                beforeSend: function() {
                    $form.addClass('sending');
                    $form.find('.invalid').removeClass('invalid');
                },
                success: function(response) {
                    $.popup({
                        classes: 'profile-updated',
                        content: response.popup
                    }).show()
                },
                error: $.parseError(function (response) {
                    if (response.errors) {
                        response.errors.forEach(function(record) {
                            var $field = $form.find('.' + record.fullname);
                            $field.addClass(record.class);
                        })
                    } else {
                        alert(window.DEFAULT_AJAX_ERROR);
                    }
                }),
                complete: function() {
                    $form.removeClass('sending');
                }
            });
        });
        //
        // $('#open-change-password').on('click', function () {
        //     $.ajax({
        //         url: window.js_storage.ajax_change_password,
        //         type: 'GET',
        //         dataType: 'json',
        //         success: function(response) {
        //             $.popup({
        //                 classes: 'profile-change-password',
        //                 content: response.popup
        //             }).show()
        //         },
        //         error: $.parseError(function () {
        //             alert(window.DEFAULT_AJAX_ERROR);
        //         })
        //     });
        // });


        $(document).on('click', '#change-password-submit', function () {
            var $form = $('#change-password-form');
            $.ajax({
                url: window.js_storage.ajax_change_password,
                type: 'POST',
                data: $form.serialize(),
                dataType: 'json',
                success: function(response) {
                    $.popup({
                        classes: 'profile-change-password-success',
                        content: response.popup
                    }).on('after_hide', function () {
                        window.location.href = response.redirect_url
                    }).show()
                },
                error: $.parseError(function (response) {
                    if (response && response.errors) {
                        $('.error-msg').html(response.errors[0]['errors'])
                    } else {
                        alert(window.DEFAULT_AJAX_ERROR);
                    }
                })
            });

            return false;
        });

        initUploader();
    });

    $('#client-profile').closest('.content').css('overflow', 'auto')

})(jQuery);