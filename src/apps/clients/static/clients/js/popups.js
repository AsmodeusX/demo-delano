(function ($) {
    $(document).ready(function () {
        $('.btn-open-register-popup').on('click', function () {
            $.ajax({
                url: window.js_storage.ajax_register,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $.popup({
                        classes: 'client-popup client-form-popup',
                        content: response.popup
                    }).on('after_show', function () {
                        var $btn = $('#client-register');

                        if($('#register_recaptcha').length) {
                            window.recaptcha_register = window.grecaptcha.render('register_recaptcha', window.captchaOptions);
                        }

                        $btn.prop('disabled', true);

                        $('#id_agree').change(function() {

                            $btn.prop('disabled', function(i, val) {
                                return !val;
                            })
                        });
                    }).show();
                },
                error: $.parseError(function (response) {
                    $(window).scrollTo( $('#prices'), 500, 'slow' );
                })
            });
        })
    });


    $(document).on('submit', '#ajax-client-register-form', function (e) {
        e.preventDefault();
        var $form = $('#ajax-client-register-form');

        if ($form.hasClass('sending')) return false;

        // добавление адреса страницы, откуда отправлена форма
        var data = $form.serializeArray();
        data.push({
            name: 'referer',
            value: location.href
        });
        data.push({
            name: 'g_recaptcha_response',
            value: window.token
        });
        $.ajax({
            url: window.js_storage.ajax_register,
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                $form.addClass('sending');
                $form.find('.invalid').removeClass('invalid');
            },
            success: function (response) {
                if (response.url) window.location.href = response.url;
            },
            error: $.parseError(function (response) {
                if (response && (response.errors || response.recaptcha_is_valid === false)) {

                    response.errors.forEach(function (record) {
                        var $field = $form.find('.' + record.fullname);
                        if ($field.length) {
                            $field.addClass(record.class);
                        }
                    });

                } else {
                    alert(window.DEFAULT_AJAX_ERROR);
                }
            }),
            complete: function () {
                $form.removeClass('sending');
                window.grecaptcha.reset(window.recaptcha_register);
            }
        });
    });

    $(document).on('click', '#client-register', function (e) {
        e.preventDefault();

        window.form = $('#client-register');
        var token = window.grecaptcha.getResponse(window.recaptcha_register);

        if (!token) {
            window.grecaptcha.execute(window.recaptcha_register);
            return false;
        }
    });
})(jQuery);