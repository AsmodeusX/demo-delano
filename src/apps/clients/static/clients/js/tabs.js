(function ($) {
    $(document).ready(function () {
        var $tabs = $('.tabs-list .tab');

        $tabs.on('click', function (e) {
            var $el = $(e.currentTarget),
                tab=  $el.data('tab');

            window.location.href = window.js_storage.portal + '?tab=' + tab;
        })
    });
})(jQuery);