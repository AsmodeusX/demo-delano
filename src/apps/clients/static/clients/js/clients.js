(function($) {

    var onLoginHandler = function(response) {
        location.reload();
    };

    var onLogoutHandler = function(response) {
        location.reload();
    };

    // ==================================
    //  Авторизация
    // ==================================

    $(document).ready(function () {
        $('#login-form').on('submit', function(e) {
            e.preventDefault();
            sendSignInForm($(this));
            return false;
        });
    });

    function sendSignInForm($form) {
        if ($form.hasClass('sending')) {
            return false;
        }

        $.ajax({
            url: $form.prop('action'),
            type: 'POST',
            data: $form.serialize(),
            dataType: 'json',
            // processData: false,
            // contentType: false,
            beforeSend: function() {
                $form.addClass('sending');
                $form.find('.invalid').removeClass('invalid');
            },
            success: function(response) {
                onLoginHandler(response);
            },
            error: $.parseError(function (response) {
                    if (response && response.errors) {
                        $('.error-msg').html(response.errors[0]['errors'])
                    } else {
                        alert(window.DEFAULT_AJAX_ERROR);
                    }
            }),
            complete: function() {
                $form.removeClass('sending');
            }
        });
    }

    // ==================================
    //  Выход из профиля
    // ==================================

    $(document).on('click', '.open-signout-popup', function() {
        showSignOutPopup();
        return false;
    });

    function showSignOutPopup() {
        /** @namespace window.ajax_views.clients.signout */
        $.preloader();

        return $.ajax({
            url: window.ajax_views.clients.signout,
            type: 'POST',
            success: function(response) {
                $.popup().hide();
                onLogoutHandler(response);
            }
        });
    }

})(jQuery);
