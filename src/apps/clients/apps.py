from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Config(AppConfig):
    name = 'clients'
    verbose_name = _('Clients')

    def ready(self):
        from django.templatetags.static import static
        from libs.js_storage import JS_STORAGE
        from django.shortcuts import resolve_url

        JS_STORAGE.update({
            'ajax_change_password': resolve_url('clients:ajax_change_password'),
            'ajax_change_bg': resolve_url('clients:ajax_change_bg'),
            'ajax_register': resolve_url('clients:ajax_register'),
            'plupload_moxie_swf': static('common/js/plupload/Moxie.swf'),
            'plupload_moxie_xap': static('common/js/plupload/Moxie.xap'),
        })