from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage
from django.templatetags.static import static
from django.contrib.auth.models import AbstractUser
from users.models import CustomUser
from . import options
from config import options as config_options
from solo.models import SingletonModel
from django.utils import timezone
from dateutil.relativedelta import relativedelta
from subscriptions import options as subscriptions_options
import string
import random


def id_generator(size=6, chars=string.digits):
    return ''.join(random.choice(chars) for x in range(size))


class ClientsConfig(SingletonModel):
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('settings')
        verbose_name_plural = _('settings')

    def __str__(self):
        return ugettext('config')


class Background(models.Model):
    config = models.ForeignKey(ClientsConfig, verbose_name=_('config'), blank=True, null=True)
    image = StdImageField(_('image'),
        storage=MediaStorage('clients/background'),
        min_dimensions=(480, 1200),
        admin_variation='admin',
        crop_area=False,
        default='',
        aspects=('desktop',),
        variations=dict(
            desktop=dict(
                size=(480, 1200),
                crop=True
            ),
            admin=dict(
                size=(80, 200),
            ),
        ),
    )

    def __str__(self):
        return ''


class Client(models.Model):
    user = models.OneToOneField(CustomUser, verbose_name=_('user'), blank=True, null=True)
    phone = models.CharField(_('phone'), max_length=64, blank=True)
    address = models.CharField(_('address'), max_length=255, blank=True)
    client_id = models.CharField(_('MyBizExpert ID'), max_length=128, unique=True, blank=True)
    company = models.CharField(_('company'), max_length=128, blank=True)
    position = models.CharField(_('position'), max_length=128, blank=True)
    stage = models.CharField(_('stage'), max_length=60, choices=config_options.BUSINESS_LIFE_STAGE, default=config_options.BUSINESS_LIFE_STAGE[0][0])
    industry = models.CharField(_('industry'), max_length=60, choices=config_options.INDUSTRY, default=config_options.INDUSTRY[0][0])

    background = models.ForeignKey(Background, verbose_name=_('background'), blank=True, null=True, on_delete=models.SET_NULL)

    photo = StdImageField(_('photo'),
                           storage=MediaStorage('clients/photos'),
                           blank=True,
                           admin_variation='normal',
                           min_dimensions=options.PHOTO_NORMAL,
                           crop_area=True,
                           aspects='normal',
                           variations=dict(
                               normal=dict(
                                   size=options.PHOTO_NORMAL,
                               ),
                               small=dict(
                                   size=options.PHOTO_SMALL,
                               ),
                           ),
                           )

    next_notify_date = models.DateTimeField(_('next email notification date'), null=True, default=None, editable=False)
    paid_until = models.DateTimeField(_('end of paid interval'), null=True, default=None, editable=False)

    class Meta:
        verbose_name = _('client')
        verbose_name_plural = _('clients')

    def __str__(self):
        return self.user.get_full_name()

    @property
    def normal_photo(self):
        if self.photo:
            return self.photo.normal.url_nocache
        else:
            return static('users/img/default_150x150.png')

    @property
    def small_photo(self):
        if self.photo:
            return self.photo.small.url_nocache
        else:
            return static('users/img/default_50x50.png')

    @staticmethod
    def gen_id(min_cnt_digit):
        max_cnt_cycles = 10
        i = 0
        clients = Client.objects.filter()

        while i < max_cnt_cycles:
            i += 1
            num = id_generator(min_cnt_digit)

            for client in clients:
                if client.id != num:
                    return num
        return False

    def save(self, *args, **kwargs):
        if not self.client_id:
            min_cnt_digit = 6
            num = self.gen_id(min_cnt_digit)

            if not num:
                min_cnt_digit += 1
                self.gen_id(min_cnt_digit)

            self.client_id = num
        super(Client, self).save(*args, **kwargs)

    def current_subscription(self):
        subscriptions = self.subscription_client.all()

        if not subscriptions.exists():
            return None

        for subscription in subscriptions:
            t = timezone.now()
            if subscription.start_date <= t < (subscription.start_date + relativedelta(months=+subscription.plan.months)):

                return subscription
        return None

    def current_plan(self):
        subscription = self.current_subscription()
        if subscription:
            return subscription.plan

        return None

    def current_package(self):
        plan = self.current_plan()
        if plan:
            return plan.package
        return None

    def active_current_subscription(self):
        if self.current_subscription():
            return self.current_subscription().status == subscriptions_options.AGREEMENT_ACTIVE or self.current_subscription().status == subscriptions_options.AGREEMENT_PENDING
        return None
