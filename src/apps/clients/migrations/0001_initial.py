# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
from django.conf import settings
import django.db.models.deletion
import libs.storages.media_storage


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Background',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('image', libs.stdimage.fields.StdImageField(variations={'admin': {'size': (80, 200)}, 'desktop': {'crop': True, 'size': (480, 1200)}}, storage=libs.storages.media_storage.MediaStorage('clients/background'), aspects=('desktop',), verbose_name='image', min_dimensions=(480, 1200), default='', upload_to='')),
            ],
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('phone', models.CharField(blank=True, max_length=64, verbose_name='phone')),
                ('address', models.CharField(blank=True, max_length=255, verbose_name='address')),
                ('client_id', models.CharField(unique=True, max_length=128, blank=True, verbose_name='MyBizExpert ID')),
                ('company', models.CharField(blank=True, max_length=128, verbose_name='company')),
                ('position', models.CharField(blank=True, max_length=128, verbose_name='position')),
                ('stage', models.CharField(choices=[('SU', 'Start up'), ('TN', 'Transition'), ('MT', 'Maturity'), ('EN', 'Expansion'), ('GT', 'Growth')], default='SU', max_length=60, verbose_name='stage')),
                ('industry', models.CharField(choices=[('AI', 'Agricultural industry'), ('AR', 'Arms industry'), ('AU', 'Automotive industry'), ('BC', 'Broadcasting Industry'), ('CH', 'Chemical industry'), ('CL', 'Clothing & apparel industry'), ('CT', 'Computer industry'), ('CN', 'Construction industry'), ('DR', 'Direct selling industry'), ('DB', 'Distribution industry'), ('ED', 'Education industry'), ('EL', 'Electronics industry'), ('EN', 'Entertainment industry'), ('FM', 'Film industry'), ('FS', 'Financial services industry'), ('FN', 'Fishing industry'), ('FD', 'Food industry'), ('HC', 'Health care industry'), ('HS', 'Hospitality industry'), ('II', 'Information industry'), ('MA', 'Manufacturing'), ('MM', 'Mass media industry'), ('MS', 'Music industry'), ('NM', 'News media industry'), ('OP', 'Other products industry'), ('OS', 'Other services industry'), ('PU', 'Publishing industry'), ('SF', 'Software industry'), ('TL', 'Telecommunications industry'), ('TR', 'Transportation industry'), ('UU', 'Unknown/unsure')], default='AI', max_length=60, verbose_name='industry')),
                ('photo', libs.stdimage.fields.StdImageField(variations={'normal': {'size': (80, 80)}, 'small': {'size': (40, 40)}}, storage=libs.storages.media_storage.MediaStorage('clients/photos'), aspects='normal', verbose_name='photo', min_dimensions=(80, 80), upload_to='', blank=True)),
                ('next_notify_date', models.DateTimeField(editable=False, null=True, default=None, verbose_name='next email notification date')),
                ('paid_until', models.DateTimeField(editable=False, null=True, default=None, verbose_name='end of paid interval')),
                ('background', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, null=True, verbose_name='background', to='clients.Background', blank=True)),
                ('user', models.OneToOneField(verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'clients',
                'verbose_name': 'client',
            },
        ),
        migrations.CreateModel(
            name='ClientsConfig',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'verbose_name_plural': 'settings',
                'verbose_name': 'settings',
            },
        ),
        migrations.AddField(
            model_name='background',
            name='config',
            field=models.ForeignKey(null=True, verbose_name='config', to='clients.ClientsConfig', blank=True),
        ),
    ]
