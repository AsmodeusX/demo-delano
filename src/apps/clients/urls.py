from django.conf.urls import url
from . import views, views_ajax
from libs.check_captcha import check_recaptcha


app_name = 'clients'
urlpatterns = [
    url(r'^ajax/profile/update/$', views_ajax.ProfileView.as_view(), name='ajax_profile'),
    url(r'^ajax/profile/change_bg/$', views_ajax.ChangeBackgroundView.as_view(), name='ajax_change_bg'),
    url(r'^ajax/profile/change_password/$', views_ajax.ChangePasswordView.as_view(), name='ajax_change_password'),
    url(r'^ajax/register/$', check_recaptcha(views_ajax.RegisterView.as_view()), name='ajax_register'),
    url(r'^ajax/login/$', views_ajax.LoginView.as_view(), name='ajax_login'),

    url(r'^ajax/photo_upload/$', views_ajax.PhotoUploadView.as_view(), name='photo_upload'),

    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^login/', views.LoginView.as_view(), name='login'),
    url(r'^change-password/', views.ChangePasswordView.as_view(), name='change_password'),
    url(r'^', views.ProfileView.as_view(), name='profile'),
]
