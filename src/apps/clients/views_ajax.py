from django.utils.translation import ugettext_lazy as _
from django.views.generic import FormView, View
from libs.views_ajax import AjaxViewMixin
from django.contrib.auth import login as auth_login
from portal.views import get_client
from users.models import CustomUser
from users.views import create_user
from django.shortcuts import resolve_url
from operator import truediv
from django.core.exceptions import ValidationError
from libs.upload import upload_chunked_file, TemporaryFileNotFoundError, NotLastChunk
from .models import Background
from .views import create_client
from .forms import LoginForm, ProfileForm, ChangePasswordForm, RegisterForm


class LoginView(AjaxViewMixin, FormView):
    form_class = LoginForm
    template_name = 'clients/pages/login.html'

    def form_valid(self, form):
        user = form.get_user()
        auth_login(self.request, user)

        return self.json_response()

    def form_invalid(self, form):
        return self.json_error({
            'errors': form.error_dict_full
        })


class RegisterView(AjaxViewMixin, View):
    def get(self, request):

        form = RegisterForm()

        return self.json_response({
            'popup': self.render_to_string('clients/popups/register.html', {
                'form': form,
            })
        })

    def post(self, request):
        data = request.POST

        form = RegisterForm(data)

        if form.is_valid() and self.request.recaptcha_is_valid:
            user = create_user(
                request,
                first_name=data.get('first_name'),
                last_name=data.get('first_name'),
                email=data.get('email')
            )
            create_client(request, user)

            return self.json_response({
                'url': resolve_url('clients:login')
            })

        else:
            return self.json_error({
                'errors': form.error_dict_full
            })


class ProfileView(AjaxViewMixin, View):
    def post(self, request):
        data = request.POST
        client = get_client(request)

        form = ProfileForm(data, instance=client)

        if form.is_valid():
            CustomUser.objects.filter(id=request.user.id).update(
                email=form.cleaned_data.get('email'),
                first_name=form.cleaned_data.get('first_name'),
                last_name=form.cleaned_data.get('last_name'),
            )

            form.save()

            return self.json_response({
                'popup': self.render_to_string('clients/popups/success.html')
            })

        else:
            return self.json_error({
                'errors': form.error_dict_full
            })


class PhotoUploadView(AjaxViewMixin, View):
    def post(self, request):
        if not request.user.is_authenticated():
            return self.json_error({
                'message': _('Authentication required'),
            }, status=401)

        try:
            uploaded_file = upload_chunked_file(request, 'image')
        except TemporaryFileNotFoundError as e:
            return self.json_error({
                'message': str(e),
            })
        except NotLastChunk:
            return self.json_response()
        client = get_client(request)

        client.photo.save(uploaded_file.name, uploaded_file, save=False)
        uploaded_file.close()

        try:
            client.full_clean()
        except ValidationError as e:
            client.photo.delete(save=False)
            return self.json_error({
                'message': ', '.join(e.messages),
            })
        else:
            client.save()

        return self.json_response({
            'small_photo': client.small_photo,
            'normal_photo': client.normal_photo,
        })


class ChangePasswordView(AjaxViewMixin, View):

    def post(self, request):
        form = ChangePasswordForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            return self.json_response({
                'redirect_url': resolve_url('clients:profile'),
                'popup': self.render_to_string('clients/popups/success.html', {
                    'form': form,
                }),
            })

        return self.json_error({
            'errors': form.error_dict_full
        })


class ChangeBackgroundView(AjaxViewMixin, View):
    def get(self, request):
        backgrounds = Background.objects.all()

        return self.json_response({
            'popup': self.render_to_string('clients/popups/change_background.html', {
                'backgrounds': backgrounds
            }),
        })

    def post(self, request):
        data = request.POST
        bg = data.get('background')
        client = get_client(request)
        client.background_id = bg
        client.save()

        return self.json_response({
            'background': self.render_to_string('portal/parts/actions_nav.html', {
                'client': client
            })
        })

