from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django.views.generic import View, FormView, TemplateView
from django.utils.http import is_safe_url
from django.shortcuts import redirect, resolve_url
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, logout as auth_logout
from seo.seo import Seo
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from .models import Client
from .forms import LoginForm, ProfileForm, ChangePasswordForm
from libs.email import send_template
from django.contrib.sites.shortcuts import get_current_site
from django.utils.safestring import mark_safe
import os


ClientModel = get_user_model()


def create_client(request, user):
    client = Client(
        phone=request.POST.get('phone'),
        address=request.POST.get('address'),
        company=request.POST.get('company'),
        position=request.POST.get('position'),
        stage=request.POST.get('stage'),
        industry=request.POST.get('industry'),
        user=user.get('user'),
    )
    client.save()
    p1 = 'https' if os.environ.get("HTTPS") == "on" else 'http'
    p2 = resolve_url('portal:index')
    send_template(request, client.user.email,
                  subject=_('Message from {domain}'),
                  template='clients/emails/register.html',
                  context={
                      'name': user.get('user').get_full_name(),
                      'login': user.get('user').username,
                      'password': user.get('pwd'),
                      'id': client.id,
                      'client': mark_safe('{}://{}{}'.format(p1, get_current_site(request).domain, resolve_url('clients:profile'))),
                      'portal': mark_safe('{}://{}{}'.format(p1, get_current_site(request).domain, p2)),
                  }
                  )

    return client

def get_client(request):
    if not request.user.is_authenticated():
        return None
    try:
        client = Client.objects.get(user_id=request.user.id)
    except ObjectDoesNotExist:
        return None

    return client


def get_redirect_url(request, default=settings.LOGIN_REDIRECT_URL):
    """
        Получение адреса для редиректа из POST, GET или settings
    """
    redirect_to = request.POST.get(
        REDIRECT_FIELD_NAME,
        request.GET.get(REDIRECT_FIELD_NAME, '')
    )
    if not is_safe_url(url=redirect_to, host=request.get_host()):
        redirect_to = resolve_url(default)
    return redirect_to


class LoginView(FormView):
    template_name = 'clients/pages/login.html'
    form_class = LoginForm

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('portal:index'))

        # Seo
        seo = Seo()
        seo.title = _('Authorization')
        seo.save(request)

        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        auth_login(self.request, form.get_client())
        return redirect(get_redirect_url(self.request))


class ChangePasswordView(TemplateView):
    template_name = 'clients/pages/change_password.html'

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(reverse('clients:login'))

        form = ChangePasswordForm(request.user)

        # Seo
        seo = Seo()
        seo.title = _('Change Password')
        seo.save(request)

        return self.render_to_response({
            'form': form
        })


class LogoutView(View):
    def get(self, request):
        auth_logout(request)

        next_page = get_redirect_url(request, default='clients:login')

        return redirect(next_page)


class ProfileView(TemplateView):
    template_name = 'clients/pages/profile.html'

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('clients:login')

        client = get_client(request)
        if not client:
            return redirect('clients:login')

        form = ProfileForm(instance=client)
        form.fields['email'].initial = client.user.email
        form.fields['first_name'].initial = client.user.first_name
        form.fields['last_name'].initial = client.user.last_name

        request.js_storage.update(
            photo_upload=resolve_url('clients:photo_upload'),
        )

        # SEO
        seo = Seo()
        seo.set({
            'title': _('Profile'),
        })
        seo.save(request)

        return self.render_to_response({
            'form': form,
            'client': client,
            'prices_link': resolve_url('index') + '#prices'
        })
