from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from project.admin import ModelAdminMixin, ModelAdminInlineMixin
from suit.admin import SortableModelAdmin, SortableStackedInline
from .models import Client, Background, ClientsConfig
from portal.models import Communication, Resource, Inquiry, Category, Query, WasteTime
from users.models import CustomUser
from solo.admin import SingletonModelAdmin
from subscriptions.models import Subscription
from libs.email import send_template
from django.forms import BaseInlineFormSet, BaseModelFormSet


class SubscriptionClient(ModelAdminInlineMixin, admin.StackedInline):
    model = Subscription
    min_num = 0
    extra = 0
    suit_classes = 'suit-tab suit-tab-subscriptions'

    readonly_fields = ('start_date', 'status', 'plan', 'subscription_id', )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class WasteTimeAdmin(ModelAdminInlineMixin, admin.StackedInline):
    model = WasteTime
    min_num = 0
    extra = 0

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class QueryClient(ModelAdminInlineMixin, admin.StackedInline):
    model = Query
    min_num = 0
    extra = 0
    suit_classes = 'suit-tab suit-tab-queries'
    readonly_fields = ('option', 'date', 'time', 'description', 'industry', 'type_consultation', 'subscription', )
    inlines = (WasteTimeAdmin, )
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

# class WasteTimeAdmin(ModelAdminMixin, admin.ModelAdmin):
#     fieldsets = (
#         (_('General info'), {
#             'classes': ('suit-tab', 'suit-tab-general'),
#             'fields': (
#                 'client_id', 'user', 'company',
#             )
#         }),
#         (_('Contact info'), {
#             'classes': ('suit-tab', 'suit-tab-general'),
#             'fields': (
#                 'phone', 'address',
#             )
#         }),
#     )
#     suit_form_tabs = (
#         ('general', _('General')),
#     )


class CommunicationClient(ModelAdminInlineMixin, admin.StackedInline):
    model = Communication
    min_num = 0
    extra = 0
    suit_classes = 'suit-tab suit-tab-communications'
    exclude = ('client_check', )


class CustomUserClient(ModelAdminInlineMixin, admin.StackedInline):
    model = CustomUser
    max_num = 1
    min_num = 1
    extra = 0
    suit_classes = 'suit-tab suit-tab-general'
    exclude = ('password', 'is_staff', 'is_superuser', 'groups', 'user_permissions', 'last_login', 'date_joined', 'username', 'avatar', 'admin', )


class ResourceClient(ModelAdminInlineMixin, SortableStackedInline):
    model = Resource
    min_num = 0
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-resources'
    exclude = ('client_check', )


class BackgroundInline(ModelAdminInlineMixin, admin.StackedInline):
    model = Background
    extra = 0
    suit_classes = 'suit-tab suit-tab-general'


class InquiryClient(ModelAdminInlineMixin, admin.StackedInline):
    model = Inquiry
    min_num = 0
    extra = 0
    suit_classes = 'suit-tab suit-tab-inquiries'
    exclude = ('answered', )
    readonly_fields = ('question', 'phase', 'category', 'client',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Client)
class ClientAdmin(ModelAdminMixin, admin.ModelAdmin):
    """ Пользователь """
    fieldsets = (
        (_('General info'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'client_id', 'user', 'company',
            )
        }),
        (_('Contact info'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'phone', 'address',
            )
        }),
    )
    inlines = (CommunicationClient, ResourceClient, InquiryClient, QueryClient, SubscriptionClient)
    suit_form_tabs = (
        ('general', _('General')),
        ('communications', _('Communications')),
        ('resources', _('Resources')),
        ('inquiries', _('Inquiries')),
        ('queries', _('Queries')),
        ('time', _('Time Queries')),
        ('subscriptions', _('Subscriptions')),
    )
    readonly_fields = ('user', )

    class Media:
        js = (
            'portal/admin/js/tabs.js',
        )


@admin.register(Category)
class CategoryAdmin(ModelAdminMixin, SortableModelAdmin):
    fieldsets = (
        (_('General info'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name',
            )
        }),
    )
    sortable = 'sort_order'
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(ClientsConfig)
class ClientsConfigAdmin(ModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (

            )
        }),
    )
    suit_form_tabs = (
        ('general', _('General')),
    )
    inlines = (BackgroundInline, )

