from django.views.generic import TemplateView
from libs.views import CachedViewMixin
from seo.seo import Seo
from .models import MainPageConfig
from packages.models import Package
from contacts.models import ContactsConfig
from clients.views import get_client
from contacts.forms import ContactForm
from portal.forms import QueryForm


class IndexView(CachedViewMixin, TemplateView):
    template_name = 'main/index.html'
    config = None

    def last_modified(self, *args, **kwargs):
        self.config = MainPageConfig.get_solo()
        return self.config.updated

    def get(self, request, *args, **kwargs):
        packages = Package.objects.filter(active=True)
        client = get_client(request) is not None
        form = ContactForm()

        # SEO
        seo = Seo()
        seo.set_data(self.config, defaults={
            'title': self.config.title,
        })
        seo.save(request)

        return self.render_to_response({
            'config': self.config,
            'client': client,
            'contacts': ContactsConfig.get_solo(),
            'form': form,
            'packages': packages,
            'query_form': str(QueryForm()),
            'is_main_page': True
        })
