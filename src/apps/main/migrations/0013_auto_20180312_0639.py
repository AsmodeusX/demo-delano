# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_auto_20180312_0637'),
    ]

    operations = [
        migrations.RenameField(
            model_name='categories',
            old_name='description',
            new_name='category_description',
        ),
        migrations.RenameField(
            model_name='categories',
            old_name='type',
            new_name='category_type',
        ),
    ]
