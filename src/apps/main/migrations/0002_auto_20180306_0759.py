# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
import libs.storages.media_storage
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Packages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('price', models.CharField(verbose_name='Price', max_length=128)),
                ('package_name', models.CharField(verbose_name='Package Name', max_length=128)),
                ('popular', models.BooleanField(verbose_name='Popular')),
                ('package_description', ckeditor.fields.CKEditorField(verbose_name='Package Description')),
                ('sort_order', models.PositiveIntegerField(verbose_name='order', default=0)),
                ('package_img', libs.stdimage.fields.StdImageField(verbose_name='Package Image', storage=libs.storages.media_storage.MediaStorage('main'), blank=True, variations={'admin': {'size': (320, 240)}, 'normal': {'size': (760, 758)}}, upload_to='', aspects=())),
            ],
            options={
                'verbose_name': 'package',
                'verbose_name_plural': 'Packages',
                'ordering': ('sort_order',),
                'default_permissions': ('delete', 'change'),
            },
        ),
        migrations.CreateModel(
            name='PackagesConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('package_header', models.CharField(verbose_name='Package Header', max_length=128)),
                ('package_text_top', models.TextField(verbose_name='Package Text Top')),
                ('package_text_bottom', models.TextField(verbose_name='Package Text Bottom')),
            ],
            options={
                'verbose_name': 'settings',
                'default_permissions': ('change',),
            },
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='billing_header',
            field=models.CharField(verbose_name='Billing Header', max_length=128, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='categories_header',
            field=models.CharField(verbose_name='Categories Header', max_length=128, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='header',
            field=models.CharField(verbose_name='Header', max_length=128, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='service_sub_1',
            field=models.CharField(verbose_name='Service Sub Title Top', max_length=32, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='service_sub_2',
            field=models.CharField(verbose_name='Service Sub Title Middle', max_length=32, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='service_sub_3',
            field=models.CharField(verbose_name='Service Sub Title Bottom', max_length=32, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='service_text',
            field=models.TextField(verbose_name='Service Text', default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='services_header',
            field=models.CharField(verbose_name='Services Header', max_length=128, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='subheader',
            field=models.CharField(verbose_name='Sub Header', max_length=256, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='text_top',
            field=ckeditor.fields.CKEditorField(verbose_name='Text Top', default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='packages',
            name='package',
            field=models.ForeignKey(related_name='packages_list', to='main.PackagesConfig'),
        ),
    ]
