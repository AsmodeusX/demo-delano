# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20180312_0634'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categories',
            name='category',
            field=models.ForeignKey(related_name='categories', to='main.MainPageConfig'),
        ),
    ]
