# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_auto_20180312_0635'),
    ]

    operations = [
        migrations.RenameField(
            model_name='categories',
            old_name='category_description',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='categories',
            old_name='category_type',
            new_name='type',
        ),
    ]
