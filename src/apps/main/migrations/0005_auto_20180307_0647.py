# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20180307_0646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mainpageconfig',
            name='service_list',
            field=ckeditor.fields.CKEditorField(verbose_name='Service List'),
        ),
    ]
