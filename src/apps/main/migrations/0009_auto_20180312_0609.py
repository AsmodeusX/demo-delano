# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20180312_0601'),
    ]

    operations = [
        migrations.RenameField(
            model_name='packages',
            old_name='package_description',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='packages',
            old_name='package_img',
            new_name='img',
        ),
        migrations.RenameField(
            model_name='packages',
            old_name='package_name',
            new_name='name',
        ),
    ]
