# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20180306_0759'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categories',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('category_type', models.CharField(max_length=128, verbose_name='Category')),
                ('category_description', ckeditor.fields.CKEditorField(verbose_name='Category Description')),
                ('sort_order', models.PositiveIntegerField(verbose_name='order', default=0)),
            ],
            options={
                'verbose_name_plural': 'Categories',
                'verbose_name': 'category',
                'default_permissions': ('delete', 'change'),
                'ordering': ('sort_order',),
            },
        ),
        migrations.CreateModel(
            name='CategoriesConfig',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('categories_header', models.CharField(max_length=128, verbose_name='Categories Header')),
            ],
            options={
                'verbose_name': 'Categories settings',
                'default_permissions': ('change',),
            },
        ),
        migrations.AlterModelOptions(
            name='packagesconfig',
            options={'verbose_name': 'Packages settings', 'default_permissions': ('change',)},
        ),
        migrations.AddField(
            model_name='categories',
            name='category',
            field=models.ForeignKey(to='main.CategoriesConfig', related_name='categories_list'),
        ),
    ]
