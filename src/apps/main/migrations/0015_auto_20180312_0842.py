# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0014_auto_20180312_0820'),
    ]

    operations = [
        migrations.AddField(
            model_name='categories',
            name='page',
            field=models.ForeignKey(to='main.MainPageConfig', related_name='categories_list', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='categories_header',
            field=models.CharField(max_length=128, verbose_name='Categories Header', default=''),
            preserve_default=False,
        ),
    ]
