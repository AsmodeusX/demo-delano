# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20180312_0609'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainpageconfig',
            name='categories_header',
            field=models.CharField(verbose_name='Categories Header', default=1, max_length=128),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='categories',
            name='category',
            field=models.ForeignKey(to='main.MainPageConfig', related_name='categories_list'),
        ),
        migrations.DeleteModel(
            name='CategoriesConfig',
        ),
    ]
