# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0017_packages_price'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='packages',
            name='package',
        ),
        migrations.DeleteModel(
            name='Packages',
        ),
    ]
