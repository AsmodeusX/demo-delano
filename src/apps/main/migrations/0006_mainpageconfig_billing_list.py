# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20180307_0647'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainpageconfig',
            name='billing_list',
            field=ckeditor.fields.CKEditorField(verbose_name='Billing List', default=1),
            preserve_default=False,
        ),
    ]
