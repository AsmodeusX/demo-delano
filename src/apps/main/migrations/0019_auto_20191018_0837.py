# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0018_auto_20190930_0407'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainpageconfig',
            name='title',
            field=models.CharField(max_length=128, default='Business Expertise', verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='mainpageconfig',
            name='header',
            field=models.TextField(default='Business Expertise', verbose_name='Header'),
        ),
    ]
