# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20180307_0443'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mainpageconfig',
            name='categories_header',
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='service_list',
            field=models.TextField(verbose_name='Service List', default=1),
            preserve_default=False,
        ),
    ]
