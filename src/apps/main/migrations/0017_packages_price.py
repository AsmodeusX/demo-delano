# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0016_remove_packages_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='packages',
            name='price',
            field=models.IntegerField(verbose_name='Price', default=50),
        ),
    ]
