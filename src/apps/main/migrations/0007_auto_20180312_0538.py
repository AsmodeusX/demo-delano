# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.sprite_image.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_mainpageconfig_billing_list'),
    ]

    operations = [
        migrations.CreateModel(
            name='Features',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('text', models.TextField(verbose_name='Billing Feature Card Text', blank=True)),
                ('icon', libs.sprite_image.fields.SpriteImageField(choices=[('clock', (-6, -178)), ('arrow', (-67, -178)), ('coin', (-132, -178)), ('hands', (-193, -178)), ('money', (-258, -178)), ('hours', (-323, -178))], size=(60, 60), verbose_name='icon', sprite='img/Sprite_A.svg', background='#000000', default='clock')),
                ('sort_order', models.PositiveIntegerField(verbose_name='order', default=0)),
            ],
            options={
                'verbose_name_plural': 'Features',
                'verbose_name': 'Feature',
                'ordering': ('sort_order',),
                'default_permissions': ('delete', 'change'),
            },
        ),
        migrations.RemoveField(
            model_name='mainpageconfig',
            name='billing_list',
        ),
        migrations.AddField(
            model_name='features',
            name='features',
            field=models.ForeignKey(to='main.MainPageConfig', related_name='features'),
        ),
    ]
