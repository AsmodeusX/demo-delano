# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_auto_20191018_0837'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainpageconfig',
            name='contact_title',
            field=models.CharField(default='What Do You Need Help With?', max_length=128, verbose_name='contact title'),
        ),
    ]
