# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20180312_0538'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainpageconfig',
            name='package_header',
            field=models.CharField(blank=True, max_length=128, verbose_name='Package Header'),
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='package_text_bottom',
            field=models.TextField(blank=True, verbose_name='Package Text Bottom'),
        ),
        migrations.AddField(
            model_name='mainpageconfig',
            name='package_text_top',
            field=models.TextField(blank=True, verbose_name='Package Text Top'),
        ),
        migrations.AlterField(
            model_name='packages',
            name='package',
            field=models.ForeignKey(related_name='packages', to='main.MainPageConfig'),
        ),
        migrations.DeleteModel(
            name='PackagesConfig',
        ),
    ]
