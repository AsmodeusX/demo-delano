from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel
from ckeditor.fields import CKEditorField
from libs.sprite_image.fields import SpriteImageField


class MainPageConfig(SingletonModel):
    """ Главная страница """
    title = models.CharField(_('Title'), max_length=128, default='Business Expertise')
    header = models.TextField(_('Header'), default='Business Expertise')
    subheader = models.CharField(_('Sub Header'), max_length=256)
    text_top = CKEditorField(_('Text Top'))

    package_header = models.CharField(_('Package Header'), max_length=128, blank=True)
    package_text_top = models.TextField(_('Package Text Top'), blank=True)
    package_text_bottom = models.TextField(_('Package Text Bottom'), blank=True)

    categories_header = models.CharField(_('Categories Header'), max_length=128)

    services_header = models.CharField(_('Services Header'), max_length=128)
    service_sub_1 = models.CharField(_('Service Sub Title Top'), max_length=32)
    service_sub_2 = models.CharField(_('Service Sub Title Middle'), max_length=32)
    service_sub_3 = models.CharField(_('Service Sub Title Bottom'), max_length=32)
    service_text = models.TextField(_('Service Text'))
    service_list = CKEditorField(_('Service List'))

    contact_title = models.CharField(_('contact title'), max_length=128, default='What Do You Need Help With?', )

    billing_header = models.CharField(_('Billing Header'), max_length=128)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        default_permissions = ('change',)
        verbose_name = _('settings')

    def __str__(self):
        return ugettext('Home page')

    def get_absolute_url(self):
        return resolve_url('index')


class Categories(models.Model):
    page = models.ForeignKey(MainPageConfig, related_name='categories_list')
    category_type = models.CharField(_('Category'), max_length=128)
    category_description = CKEditorField(_('Category Description'))
    sort_order = models.PositiveIntegerField(_('order'), default=0)

    class Meta:
        default_permissions = ('delete', 'change')
        verbose_name = _('category')
        verbose_name_plural = _('Categories')
        ordering = ('sort_order',)

    def __str__(self):
        return self.category_type


class Features(models.Model):
    features = models.ForeignKey(MainPageConfig, related_name='features')
    text = models.TextField(_('Billing Feature Card Text'), blank=True)
    ICONS = (
        ('clock', (-6, -178)),
        ('arrow', (-67, -178)),
        ('coin', (-132, -178)),
        ('hands', (-193, -178)),
        ('money', (-258, -178)),
        ('hours', (-323, -178)),
    )
    icon = SpriteImageField(
        _('icon'),
        sprite='img/Sprite_A.svg',
        size=(60, 60),
        choices=ICONS,
        default=ICONS[0][0],
        background='#000000',
    )
    sort_order = models.PositiveIntegerField(_('order'), default=0)

    class Meta:
        default_permissions = ('delete', 'change')
        verbose_name = _('Feature')
        verbose_name_plural = _('Features')
        ordering = ('sort_order',)

    def __str__(self):
        return self.text
