(function($) {
    var captcha_loaded;

    $(document).ready(function(){
        /* делаем плавное появление */
        $(".btn-hero-white, .btn-hero-red, .text-main").fadeIn('slow');
    });

    $(document).ready(function() {
        var $prices = $('.price-photo');

        // slider
        $slick_slider = $('.prices-photo-container');

        var settings_slider = {
            arrows: false,
            centerMode: true,
            centerPadding: '20px',
            slidesToShow: 1,
            infinite: true
        };

        $(window).resize(function() {
            ($slick_slider).slick('resize');
        });


        slick_on_mobile( $slick_slider, settings_slider);


        // slick on mobile
        function slick_on_mobile(slider, settings){
            $(window).on('load resize', function() {
                if ($(window).width() >= 755) {
                    if (slider.hasClass('slick-initialized')) {
                        slider.slick('unslick');
                    }
                    return
                }
                if (!slider.hasClass('slick-initialized')) {
                    return slider.slick(settings);
                }
            });
        }

        $('.ca3-scroll-down-arrow, .btn-anchor--prices').click(function(e) {
            e.preventDefault();
            $(window).scrollTo( $('#prices'), 500, 'slow' );
            // $('body, html').animate({scrollTop: $destination}, 'slow');
        });

        // button

        // Находим элемент
        var show = $('.cuarter-show');

        // Функция
        function showList() {

            // добавляем класс актив
            $('.price-list').toggleClass('active');
        }

        // Применяем клик

        show.on('click', showList);

        //pre-loader

        var $slick_slider = $(".prices-photo-container").slick({
            focusOnSelect: true,
            adaptiveHeight: true
        });

        $slick_slider.find(".price-img, .price-title, .price-mo").on("click", function(){
            $slick_slider.slick("slickNext");
        });

        $('.social-footer-links a').on('click', function() {

            var otherWindow = window.open();
            otherWindow.opener = null;
            otherWindow.location = $(this).attr('href');

        });

        $prices
            .on('mouseenter', function() {
                $(this).find('.btn').addClass('btn-green-active')
            })
            .on('mouseleave', function() {
                $(this).find('.btn').removeClass('btn-green-active')
            });

        $(window).on('mousemove', function () {
            get_captcha();
        });

        $(document).on('keydown', function () {
            get_captcha();
        });

        function get_captcha() {
            if (!(captcha_loaded)) {
                $.getScript('https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit&hl=en');
                captcha_loaded = true;
            }
        }
    });
})(jQuery);
