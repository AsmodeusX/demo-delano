from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from seo.admin import SeoModelAdminMixin
from attachable_blocks.admin import AttachedBlocksStackedInline
from project.admin import ModelAdminMixin
from suit.admin import SortableStackedInline
from .models import MainPageConfig, Categories, Features


class FeaturesAdmin(ModelAdminMixin, SortableStackedInline):
    model = Features
    min_num = 1
    max_num = 6
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-features-list'


class CategoriesAdmin(ModelAdminMixin, SortableStackedInline):
    model = Categories
    extra = 0
    min_num = 1
    max_num = 3
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-categories'


@admin.register(MainPageConfig)
class MainPageConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    """ Главная страница """
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'header', 'subheader', 'text_top',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'package_header', 'package_text_top', 'package_text_bottom',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'categories_header',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-services'),
            'fields': (
                'services_header', 'service_sub_1', 'service_sub_2', 'service_sub_3', 'service_text', 'service_list',
            ),
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-features-list'),
            'fields': (
                'billing_header',
            ),
        }),
    )
    inlines = (FeaturesAdmin, CategoriesAdmin )
    suit_form_tabs = (
        ('general', _('General')),
        ('services', _('Services')),
        ('features-list', _('Billing')),
        ('packs-list', _('Package')),
        ('categories', _('Categories')),
        ('blocks', _('Blocks')),
    )
