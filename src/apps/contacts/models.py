from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel
from attachable_blocks.models import AttachableBlock
from google_maps.fields import GoogleCoordsField


class ContactsConfig(SingletonModel):
    phone = models.CharField(_('phone'), max_length=64, default='1-877-692-4990')
    phone_mask = models.CharField(_('phone mask'), max_length=64, default='1-877-MyBizX0')

    address = models.CharField(_('address'), max_length=255, default='')
    city = models.CharField(_('city'), max_length=255, default='')
    region = models.CharField(_('region'), max_length=64, blank=True)
    zip = models.CharField(_('zip'), max_length=32, blank=True)
    coords = GoogleCoordsField(_('coords'), blank=True)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        default_permissions = ('change', )
        verbose_name = _('settings')

    def __str__(self):
        return ugettext('Contact page')

    def str_address(self):
        return '{}, {}, {} {}'.format(self.address, self.city, self.region, self.zip)


class NotificationReceiver(models.Model):
    """ Получатели писем с информацией о отправленных сообщениях """
    config = models.ForeignKey(ContactsConfig, related_name='receivers')
    email = models.EmailField(_('e-mail'))

    class Meta:
        verbose_name = _('notification receiver')
        verbose_name_plural = _('notification receivers')

    def __str__(self):
        return self.email


class Message(models.Model):
    """ Сообщение """
    name = models.CharField(_('name'), max_length=128)
    phone = models.CharField(_('phone'), max_length=32, blank=True)
    email = models.EmailField(_('e-mail'), blank=True)
    message = models.TextField(_('message'), max_length=2048)
    date = models.DateTimeField(_('date sent'), default=now, editable=False)
    referer = models.CharField(_('from page'), max_length=512, blank=True, editable=False)

    class Meta:
        default_permissions = ('delete', )
        verbose_name = _('message')
        verbose_name_plural = _('messages')
        ordering = ('-date',)

    def __str__(self):
        return self.name
