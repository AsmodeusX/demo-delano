from django.conf.urls import url
from . import views_ajax
from libs.check_captcha import check_recaptcha


urlpatterns = [
    url(r'^ajax/$', check_recaptcha(views_ajax.ContactView.as_view()), name='ajax_contact'),
]
