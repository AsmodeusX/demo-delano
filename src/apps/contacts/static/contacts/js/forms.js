(function($) {
    var recaptchaId;

    /** @namespace window.js_storage.ajax_contact */


    $(document).on('submit', '#contact-form', function (e) {
        e.preventDefault();
        var $form = $('#contact-form');

        if ($form.hasClass('sending')) return false;

        // добавление адреса страницы, откуда отправлена форма
        var data = $form.serializeArray();
        data.push({
            name: 'referer',
            value: location.href
        });
        data.push({
            name: 'g_recaptcha_response',
            value: window.token
        });
        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                $form.addClass('sending');
                $form.find('.invalid').removeClass('invalid');
            },
            success: function (response) {
                $.popup({
                    classes: 'contact-popup contact-success-popup',
                    content: response.popup
                }).show();
                $form.get(0).reset()
            },
            error: $.parseError(function (response) {
                if (response && (response.errors || response.recaptcha_is_valid === false)) {
                    // ошибки формы


                    response.errors.forEach(function (record) {
                        var $field = $form.find('.' + record.fullname);
                        if ($field.length) {
                            $field.addClass(record.class);
                        }
                    });


                } else {
                    alert(window.DEFAULT_AJAX_ERROR);
                }
            }),
            complete: function () {
                $form.removeClass('sending');
                window.grecaptcha.reset(recaptchaId);
            }
        });
    });

    window.recaptchaCallback = function() {
        var captchaOptions = {
            sitekey: document.documentElement.getAttribute('data-google-recaptcha-key'),
            size: 'invisible',
            callback: window.fnSend
        };

        if ($('#recaptcha').length) {
            recaptchaId = window.grecaptcha.render('recaptcha', captchaOptions);
        }
    };

    $(document).on('click', '#send-contact-form', function (e) {
        window.form = $(e.target).parent('form');
        var token = window.grecaptcha.getResponse(window.recaptcha_contact);
        if (!token) {
            window.grecaptcha.execute(window.recaptcha_contact);
            return false;
        }
    });



    $(document).ready(function () {
        $('#send-contact-form').on('click', function (e) {
            e.preventDefault();
            var token = window.grecaptcha.getResponse(recaptchaId);
            if (token.length === 0) {
                window.grecaptcha.execute(recaptchaId);
            }
        });
    });

})(jQuery);
