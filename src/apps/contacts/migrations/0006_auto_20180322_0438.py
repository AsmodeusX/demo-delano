# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0005_auto_20180222_0245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buyrequest',
            name='plan_select',
            field=models.CharField(verbose_name='Payment plan', max_length=60),
        ),
    ]
