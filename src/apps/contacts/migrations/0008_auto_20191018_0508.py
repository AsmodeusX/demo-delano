# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import google_maps.fields


class Migration(migrations.Migration):

    dependencies = [
        ('attachable_blocks', '0005_auto_20180122_0520'),
        ('contacts', '0007_delete_buyrequest'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contactblock',
            name='attachableblock_ptr',
        ),
        migrations.RemoveField(
            model_name='openinghours',
            name='address',
        ),
        migrations.RemoveField(
            model_name='phonenumber',
            name='address',
        ),
        migrations.RemoveField(
            model_name='contactsconfig',
            name='header',
        ),
        migrations.AddField(
            model_name='contactsconfig',
            name='address',
            field=models.CharField(default='', verbose_name='address', max_length=255),
        ),
        migrations.AddField(
            model_name='contactsconfig',
            name='city',
            field=models.CharField(default='', verbose_name='city', max_length=255),
        ),
        migrations.AddField(
            model_name='contactsconfig',
            name='coords',
            field=google_maps.fields.GoogleCoordsField(help_text='Double click on the map places marker', blank=True, verbose_name='coords'),
        ),
        migrations.AddField(
            model_name='contactsconfig',
            name='phone',
            field=models.CharField(default='1-877-692-4990', verbose_name='phone', max_length=64),
        ),
        migrations.AddField(
            model_name='contactsconfig',
            name='phone_mask',
            field=models.CharField(default='1-877-MyBizX0', verbose_name='phone mask', max_length=64),
        ),
        migrations.AddField(
            model_name='contactsconfig',
            name='region',
            field=models.CharField(blank=True, verbose_name='region', max_length=64),
        ),
        migrations.AddField(
            model_name='contactsconfig',
            name='zip',
            field=models.CharField(blank=True, verbose_name='zip', max_length=32),
        ),
        migrations.DeleteModel(
            name='Address',
        ),
        migrations.DeleteModel(
            name='ContactBlock',
        ),
        migrations.DeleteModel(
            name='OpeningHours',
        ),
        migrations.DeleteModel(
            name='PhoneNumber',
        ),
    ]
