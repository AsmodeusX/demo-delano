# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0004_buyrequest'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buyrequest',
            name='industry_type',
            field=models.CharField(choices=[('AI', 'agricultural industry'), ('AR', 'arms industry'), ('AU', 'automotive industry'), ('BC', 'broadcasting Industry'), ('CH', 'chemical industry'), ('CL', 'clothing & apparel industry'), ('CT', 'computer industry'), ('CN', 'construction industry'), ('DR', 'direct selling industry'), ('DB', 'distribution industry'), ('ED', 'education industry'), ('EL', 'electronics industry'), ('EN', 'entertainment industry'), ('FM', 'film industry'), ('FS', 'financial services industry'), ('FN', 'fishing industry'), ('FD', 'food industry'), ('HC', 'health care industry'), ('HS', 'hospitality industry'), ('II', 'information industry'), ('MA', 'manufacturing'), ('MM', 'mass media industry'), ('MS', 'music industry'), ('NM', 'news media industry'), ('OP', 'other products industry'), ('OS', 'other services industry'), ('PU', 'publishing industry'), ('SF', 'software industry'), ('TL', 'telecommunications industry'), ('TR', 'transportation industry'), ('UU', 'unknown/unsure')], default='AI', max_length=60),
        ),
    ]
