# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0003_openinghours'),
    ]

    operations = [
        migrations.CreateModel(
            name='BuyRequest',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='name')),
                ('last_name', models.CharField(max_length=128, verbose_name='last name')),
                ('email', models.EmailField(max_length=254, blank=True, verbose_name='e-mail')),
                ('phone', models.CharField(max_length=32, blank=True, verbose_name='phone')),
                ('address', models.CharField(max_length=128, verbose_name='address')),
                ('company_name', models.CharField(max_length=128, blank=True, verbose_name='company name')),
                ('position', models.CharField(max_length=128, blank=True, verbose_name='position')),
                ('life_stage', models.CharField(default='SU', max_length=60, choices=[('SU', 'start up'), ('TN', 'transition'), ('MT', 'maturity'), ('EN', 'expansion'), ('GT', 'growth')])),
                ('plan_select', models.CharField(default='SX', max_length=60, choices=[('SX', '$55/month for 6 month subscription'), ('TW', '$50/month when you choose 12 month subscription')])),
                ('industry_type', models.CharField(default='SU', max_length=60, choices=[('SU', 'start up'), ('TN', 'transition'), ('MT', 'maturity'), ('EN', 'expansion'), ('GT', 'growth')])),
                ('date', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='date sent')),
            ],
            options={
                'verbose_name_plural': 'buy requests',
                'ordering': ('-date',),
                'default_permissions': ('delete',),
                'verbose_name': 'buy request',
            },
        ),
    ]
