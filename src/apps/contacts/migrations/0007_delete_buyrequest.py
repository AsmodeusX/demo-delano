# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0006_auto_20180322_0438'),
    ]

    operations = [
        migrations.DeleteModel(
            name='BuyRequest',
        ),
    ]
