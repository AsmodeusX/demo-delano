from django.db import models
from django.db.models.base import ModelBase
from django.utils.translation import ugettext_lazy as _


class StdPortalFieldsMetaClass(ModelBase):
    def __new__(cls, name, bases, attrs):
        cls = super().__new__(cls, name, bases, attrs)
        return cls


class StdPortalFields(models.Model):
    title = models.CharField(_('Header'), max_length=128)
    header = models.CharField(_('Header'), max_length=128, blank=True)

    updated = models.DateTimeField(_('change date'), auto_now=True, blank=True,)

    class Meta:
        abstract = True
        verbose_name = _('settings')
