from django.utils.translation import ugettext_lazy as _
from seo.admin import SeoModelAdminMixin


class StdPageAdmin(SeoModelAdminMixin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'header',
            ),
        }),
    )
    suit_form_tabs = (
        ('general', _('General')),
    )
