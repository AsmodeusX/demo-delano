from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import CustomUser, AdminUser
from project.admin import ModelAdminMixin


@admin.register(CustomUser)
class CustomUserAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('username', 'password'),
        }),
        (_('Personal info'), {
            'fields': ('first_name', 'last_name', 'email',),
        }),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {
            'fields': ('last_login', 'date_joined'),
        }),
    )

@admin.register(AdminUser)
class AdminUserAdmin(ModelAdminMixin, admin.ModelAdmin):
    pass
