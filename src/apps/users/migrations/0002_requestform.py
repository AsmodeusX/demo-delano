# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RequestForm',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('business_phase', models.CharField(choices=[('SU', 'start up'), ('TN', 'transition'), ('MT', 'maturity'), ('EN', 'expansion'), ('GT', 'growth')], max_length=60, default='SU')),
                ('industry_type', models.CharField(choices=[('AI', 'agricultural industry'), ('AR', 'arms industry'), ('AU', 'automotive industry'), ('BC', 'broadcasting Industry'), ('CH', 'chemical industry'), ('CL', 'clothing & apparel industry'), ('CT', 'computer industry'), ('CN', 'construction industry'), ('DR', 'direct selling industry'), ('DB', 'distribution industry'), ('ED', 'education industry'), ('EL', 'electronics industry'), ('EN', 'entertainment industry'), ('FM', 'film industry'), ('FS', 'financial services industry'), ('FN', 'fishing industry'), ('FD', 'food industry'), ('HC', 'health care industry'), ('HS', 'hospitality industry'), ('II', 'information industry'), ('MA', 'manufacturing'), ('MM', 'mass media industry'), ('MS', 'music industry'), ('NM', 'news media industry'), ('OP', 'other products industry'), ('OS', 'other services industry'), ('PU', 'publishing industry'), ('SF', 'software industry'), ('TL', 'telecommunications industry'), ('TR', 'transportation industry'), ('UU', 'unknown/unsure')], max_length=60, default='AI')),
                ('message', models.TextField(max_length=2048, verbose_name='message')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='date sent')),
            ],
            options={
                'verbose_name_plural': 'request form',
                'ordering': ('-date',),
                'default_permissions': ('delete',),
                'verbose_name': 'request form',
            },
        ),
    ]
