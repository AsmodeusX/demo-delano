# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_requestform'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdminUser',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
            ],
            options={
                'ordering': ('user',),
                'verbose_name_plural': 'managers',
                'verbose_name': 'manager',
            },
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='avatar',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='avatar_crop',
        ),
        migrations.AlterField(
            model_name='requestform',
            name='business_phase',
            field=models.CharField(choices=[('SU', 'Start up'), ('TN', 'Transition'), ('MT', 'Maturity'), ('EN', 'Expansion'), ('GT', 'Growth')], default='SU', max_length=60),
        ),
        migrations.AlterField(
            model_name='requestform',
            name='industry_type',
            field=models.CharField(choices=[('AI', 'Agricultural industry'), ('AR', 'Arms industry'), ('AU', 'Automotive industry'), ('BC', 'Broadcasting Industry'), ('CH', 'Chemical industry'), ('CL', 'Clothing & apparel industry'), ('CT', 'Computer industry'), ('CN', 'Construction industry'), ('DR', 'Direct selling industry'), ('DB', 'Distribution industry'), ('ED', 'Education industry'), ('EL', 'Electronics industry'), ('EN', 'Entertainment industry'), ('FM', 'Film industry'), ('FS', 'Financial services industry'), ('FN', 'Fishing industry'), ('FD', 'Food industry'), ('HC', 'Health care industry'), ('HS', 'Hospitality industry'), ('II', 'Information industry'), ('MA', 'Manufacturing'), ('MM', 'Mass media industry'), ('MS', 'Music industry'), ('NM', 'News media industry'), ('OP', 'Other products industry'), ('OS', 'Other services industry'), ('PU', 'Publishing industry'), ('SF', 'Software industry'), ('TL', 'Telecommunications industry'), ('TR', 'Transportation industry'), ('UU', 'Unknown/unsure')], default='AI', max_length=60),
        ),
        migrations.AddField(
            model_name='adminuser',
            name='user',
            field=models.OneToOneField(null=True, verbose_name='user', default='', to=settings.AUTH_USER_MODEL),
        ),
    ]
