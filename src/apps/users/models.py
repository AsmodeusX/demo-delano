from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from config import options as config_options


class RequestForm(models.Model):
    """ RequestInquiryForm """

    business_phase = models.CharField(max_length=60, choices=config_options.BUSINESS_LIFE_STAGE, default=config_options.BUSINESS_LIFE_STAGE[0][0])

    industry_type = models.CharField(max_length=60, choices=config_options.INDUSTRY, default=config_options.INDUSTRY[0][0])
    message = models.TextField(_('message'), max_length=2048)

    date = models.DateTimeField(_('date sent'), default=now, editable=False)

    class Meta:
        default_permissions = ('delete',)
        verbose_name = _('request form')
        verbose_name_plural = _('request form')
        ordering = ('-date',)

    def __str__(self):
        return self.message


class CustomUser(AbstractUser):
    class Meta(AbstractUser.Meta):
        permissions = (
            ('admin_menu', 'Can see hidden menu items'),
        )


class AdminUser(models.Model):
    user = models.OneToOneField(CustomUser, verbose_name=_('user'), default='', null=True)

    class Meta:
        verbose_name = _('manager')
        verbose_name_plural = _('managers')
        ordering = ('user', )

    def __str__(self):
        return self.user.get_full_name()