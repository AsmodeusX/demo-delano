# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Privacy',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(default='', max_length=128, verbose_name='title')),
                ('header', models.TextField(default='', verbose_name='header')),
                ('text_field', ckeditor.fields.CKEditorField(verbose_name='page text content')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'verbose_name': 'Privacy Policy',
            },
        ),
        migrations.CreateModel(
            name='Terms',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(default='', max_length=128, verbose_name='title')),
                ('header', models.TextField(default='', verbose_name='header')),
                ('text_field', ckeditor.fields.CKEditorField(verbose_name='page text content')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'verbose_name': 'Terms of Use',
            },
        ),
    ]
