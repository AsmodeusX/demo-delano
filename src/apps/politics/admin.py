from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from seo.admin import SeoModelAdminMixin
from .models import Privacy, Terms


class PoliticsConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'header', 'text_field',
            ),
        }),
    )
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(Privacy)
class PrivacyAdmin(PoliticsConfigAdmin):
    fieldsets = PoliticsConfigAdmin.fieldsets
    suit_form_tabs = PoliticsConfigAdmin.suit_form_tabs


@admin.register(Terms)
class TermsAdmin(PoliticsConfigAdmin):
    fieldsets = PoliticsConfigAdmin.fieldsets
    suit_form_tabs = PoliticsConfigAdmin.suit_form_tabs