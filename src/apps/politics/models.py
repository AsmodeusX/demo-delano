from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel
from ckeditor.fields import CKEditorField


class PoliticsConfig(SingletonModel):
    title = models.CharField(_('title'), max_length=128, default='')
    header = models.TextField(_('header'), default='')
    text_field = CKEditorField(_('page text content'))
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        abstract = True


class Privacy(PoliticsConfig):

    class Meta:
        verbose_name = _('Privacy Policy')

    def get_absolute_url(self):
        return resolve_url('privacy')

    def __str__(self):
        return ugettext('Privacy Policy')


class Terms(PoliticsConfig):

    class Meta:
        verbose_name = _('Terms of Use')

    def get_absolute_url(self):
        return resolve_url('terms')

    def __str__(self):
        return ugettext('Terms Of Use')