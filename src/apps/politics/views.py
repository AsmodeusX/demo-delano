from django.views.generic import TemplateView
from libs.views import CachedViewMixin
from seo.seo import Seo
from .models import Privacy, Terms


class PolicyView(CachedViewMixin, TemplateView):
    template_name = 'politics/page.html'
    config = None

    def last_modified(self, *args, **kwargs):
        self.config = Privacy.get_solo()
        return self.config.updated

    def get(self, request, *args, **kwargs):
        # SEO
        seo = Seo()
        seo.set_data(self.config, defaults={
            'title': self.config.title,
        })
        seo.save(request)

        return self.render_to_response({
            'config': self.config
        })


class TermsView(CachedViewMixin, TemplateView):
    template_name = 'politics/page.html'
    config = None

    def last_modified(self, *args, **kwargs):
        self.config = Terms.get_solo()
        return self.config.updated

    def get(self, request, *args, **kwargs):
        # SEO
        seo = Seo()
        seo.set_data(self.config, defaults={
            'title': self.config.title,
        })
        seo.save(request)

        return self.render_to_response({
            'config': self.config
        })