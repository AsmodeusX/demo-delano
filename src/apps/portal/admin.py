from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import (PortalConfig, Question, TypeConsultation, Inquiry,
                     AnswerInquiry, Query, WasteTime, NotificationReceiver)
from solo.admin import SingletonModelAdmin
from suit.admin import SortableModelAdmin
from project.admin.base import ModelAdminMixin, ModelAdminInlineMixin



class NotificationReceiverInline(ModelAdminInlineMixin, admin.StackedInline):
    model = NotificationReceiver
    extra = 0
    suit_classes = 'suit-tab suit-tab-notifications'


class TypeConsultationInline(ModelAdminInlineMixin, admin.StackedInline):
    model = TypeConsultation
    extra = 0
    suit_classes = 'suit-tab suit-tab-types'


class WasteTimeInline(ModelAdminInlineMixin, admin.StackedInline):
    model = WasteTime
    extra = 0
    max_num = 1
    suit_classes = 'suit-tab suit-tab-general'

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(PortalConfig)
class PortalConfigAdmin(ModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
        (_('FAQ'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'faq_header',
            ),
        }),
    )
    inlines = (NotificationReceiverInline, TypeConsultationInline, )

    suit_form_tabs = (
        ('general', _('General')),
        ('notifications', _('Notifications')),
        ('types', _('Types Consultations')),
    )


class AnswerInquiryInline(ModelAdminInlineMixin, admin.StackedInline):
    model = AnswerInquiry
    min_num = 1
    max_num = 1
    extra = 0
    suit_classes = 'suit-tab suit-tab-general'
    readonly_fields = ('send', )
    exclude = ('client_check', )

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Question)
class QuestionAdmin(ModelAdminMixin, SortableModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'question', 'answer',
            ),
        }),
    )
    sortable = 'sort_order'
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(Inquiry)
class InquiryAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'question', 'phase', 'category', 'client',
            ),
        }),
    )
    readonly_fields = ('question', 'phase', 'category', 'client',)
    suit_form_tabs = (
        ('general', _('General')),
    )
    inlines = (AnswerInquiryInline, )

    def has_add_permission(self, request):
        return False


@admin.register(Query)
class QueryAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'option', 'client', 'date', 'time', 'description', 'subscription',
            ),
        }),
    )
    list_display = ('option', 'client', 'date', 'time', )
    list_filter = ('option',)
    search_fields = ('client', 'date', )
    readonly_fields = ('option', 'client', 'date', 'time', 'description', 'subscription', )
    inlines = (WasteTimeInline, )
    suit_form_tabs = (
        ('general', _('General')),
    )

    def has_add_permission(self, request):
        return False
