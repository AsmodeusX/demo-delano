from django.conf.urls import url
from . import views, views_ajax

urlpatterns = [
    url(r'^ajax/query/', views_ajax.QueryView.as_view(), name='ajax_query'),
    url(r'^ajax/count-items/', views_ajax.CountItemsView.as_view(), name='ajax_count_items'),
    url(r'^ajax/inquiry/', views_ajax.InquiryView.as_view(), name='ajax_inquiry'),
    url(r'^$', (views.IndexView.as_view()), name='index'),
]