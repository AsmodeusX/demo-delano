from django import forms
from django.utils.translation import ugettext_lazy as _
from libs.form_helper.forms import FormHelperMixin
from .models import Inquiry
from .models import Query
import datetime
import re
from .options import TIME_OPTIONS


class InquiryForm(FormHelperMixin, forms.ModelForm):
    default_field_template = 'form_helper/unlabeled_field.html'
    # client = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Inquiry
        fields = ('question', 'category', 'phase', )
        widgets = {
            'question': forms.Textarea(attrs={
                'rows': 5,
                'placeholder': _('In a few words, describe your inquiry or business need in detail'),
            }),
            'category': forms.Select(attrs={
                'placeholder': _('Choose Your Topic'),
            }),
            'phase': forms.Select(attrs={
                'placeholder': _('Choose Your Business Phase'),
            }),
        }
        error_messages = {
            'question': {
                'required': _('Please enter your question'),
                'min_length': _('Your question must contain at least 3 letters'),
                'max_length': _('Question should not be longer than %(limit_value)d characters'),
                'incorrect': _('Your question contains illegal characters'),
            },
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean_question(self):
        if 'question' in self.cleaned_data:
            question = self.cleaned_data.get('question')

            if not question:
                self.add_field_error('question', 'required')
            else:

                r = re.findall(r'[а-яА-я]', question)

                if len(r) > 0:
                    self.add_field_error('question', 'incorrect')

                r = re.findall(r'[a-zA-Z]', question)

                if len(r) < 3:
                    self.add_field_error('question', 'min_length')

            return question


class QueryForm(FormHelperMixin, forms.ModelForm):
    default_field_template = 'form_helper/unlabeled_field.html'
    # client = forms.CharField(widget=forms.HiddenInput())
    field_templates = {
        'date': {
            'template': 'form_helper/date_field.html',
        },
    }

    time = forms.ChoiceField(
        widget=forms.Select(attrs={
            'placeholder': _('Select a call-back time frame'),
        }),
        required=True,
        choices=TIME_OPTIONS,
        error_messages = ({
            'old_date': _('Date due must not be less than the current'),
            'required': _('Please select time'),
        })
    )

    date = forms.DateField(
        widget=forms.DateInput(attrs={
            'required': True,
            'class': "datepicker",
            'readonly': 'readonly',
            'placeholder': _('Select a date'),
        }),
        error_messages=({
            'old_date': _('Date due must not be less than the current'),
            'required': _('Please enter correct date due'),
        })
    )


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['time'].choices = TIME_OPTIONS


    class Meta:
        model = Query
        fields = ('date', 'time', 'description', )
        widgets = {
            'description': forms.Textarea(attrs={
                'placeholder': _('In a few words, describe the topics you would like to discuss during your call'),
                'rows': 4,
            }),
        }
        error_messages = {
            'question': {
                'required': _('Please enter your question'),
                'min_length': _('Your question must contain at least 3 letters'),
                'max_length': _('Question should not be longer than %(limit_value)d characters'),
                'incorrect': _('Your question contains illegal characters'),
            }
        }

    def clean_question(self):
        if 'question' in self.cleaned_data:
            question = self.cleaned_data.get('question')

            if not question:
                self.add_field_error('question', 'required')
            else:

                r = re.findall(r'[а-яА-я]', question)

                if len(r) > 0:
                    self.add_field_error('question', 'incorrect')

                r = re.findall(r'[a-zA-Z]', question)

                if len(r) < 3:
                    self.add_field_error('question', 'min_length')

            return question

    def clean_date(self):
        if 'date' in self.cleaned_data:
            date = self.cleaned_data.get('date')
            if date:
                if date < datetime.datetime.now().date():
                    return self.add_field_error('date', 'old_date')
            else:
                return self.add_field_error('date', 'required')
            return date

    def clean_time(self):
        if 'time' in self.cleaned_data:
            time = self.cleaned_data.get('time')
            if not time:
                return self.add_field_error('time', 'required')
            return time


class ExtraForm(QueryForm):

    class Meta(QueryForm.Meta):
        fields = ('date', 'time', 'description', 'type_consultation',  'industry', )
        widgets = {
            'type_consultation': forms.Select(),
            'industry': forms.Select(),
            'description': forms.Textarea(attrs={
                'placeholder': _('In a few words, describe the topics you would like to discuss during your call'),
            }),
        }