from django.utils.translation import ugettext_lazy as _


ICONS = (
    ('phone', (-138, -111)),
    ('consultation', (-230, -111)),
    ('plan', (-184, -111)),
    ('video', (-0, -111)),
    ('chat', (-46, -111)),
    ('message', (-92, -111)),
)


TIME_AM_0002 = 1
TIME_AM_0204 = 2
TIME_AM_0406 = 3
TIME_AM_0608 = 4
TIME_AM_0810 = 5
TIME_AM_1012 = 6

TIME_PM_0002 = 7
TIME_PM_0204 = 8
TIME_PM_0406 = 9
TIME_PM_0608 = 10
TIME_PM_0810 = 11
TIME_PM_1012 = 12

TIME_OPTIONS = (
    (TIME_AM_0002, _('00:00AM - 02:00AM')),
    (TIME_AM_0204, _('02:00AM - 04:00AM')),
    (TIME_AM_0406, _('04:00AM - 06:00AM')),
    (TIME_AM_0608, _('06:00AM - 08:00AM')),
    (TIME_AM_0810, _('08:00AM - 10:00AM')),
    (TIME_AM_1012, _('10:00AM - 12:00AM')),

    (TIME_PM_0002, _('00:00PM - 02:00PM')),
    (TIME_PM_0204, _('02:00PM - 04:00PM')),
    (TIME_PM_0406, _('04:00PM - 06:00PM')),
    (TIME_PM_0608, _('06:00PM - 08:00PM')),
    (TIME_PM_0810, _('08:00PM - 10:00PM')),
    (TIME_PM_1012, _('10:00PM - 12:00PM')),
)
