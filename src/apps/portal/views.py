from django.views.generic import TemplateView

from libs.views import CachedViewMixin
from seo.seo import Seo
from .models import PortalConfig, Question, Inquiry, Communication, Resource
from django.shortcuts import redirect, resolve_url, Http404
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from .forms import InquiryForm
from .models import WasteTime
from paypal import api as paypal_api
from clients.views import get_client
from paypal.api import get_subscription
from subscriptions.models import Transaction
from django.db.models import Q
import logging


logger = logging.getLogger('django.paypal')


def return_prices():
    return redirect('{}#{}'.format(resolve_url('index'), 'prices'))


# Обновляем количество ответов по каждому типу обращений клиенту
def get_new_counts(client):
    inquiries = Inquiry.objects.filter(client_id=client.id)
    communications = Communication.objects.filter(client_id=client.id)
    resources = Resource.objects.filter(client_id=client.id)

    new_counts = {
        'inquiries_new_count': 0,
        'communications_new_count': 0,
        'resources_new_count': 0,
    }

    for inquiry in inquiries:
        try:
            if not inquiry.answer_inquiry.client_check:
                new_counts['inquiries_new_count'] += 1
        except ObjectDoesNotExist:
            pass

    for communication in communications:
        if not communication.client_check:
            new_counts['communications_new_count'] += 1

    for resource in resources:
        if not resource.client_check:
            new_counts['resources_new_count'] += 1

    return new_counts


def check_subscription(request):
    client = get_client(request)

    paypal_api.define_config()

    current_subscription = client.current_subscription()

    # Пробуем извлечь ее идентификатор, иначе пользователь не дошел до записи в базу(не довел оплату до конца)
    # if current_subscription.subscription_id:
    try:
        if current_subscription and client.active_current_subscription():
            subscription_id = current_subscription.subscription_id
            subscription = get_subscription(subscription_id)
            if subscription:
                if subscription.error:
                    logger.error('Paypal subscription not found: {%s}' % subscription.error)
                else:
                    paypal_state = subscription.status.upper()

                    # Обновляем данные о состоянии оплаты
                    if paypal_state != client.subscription_client.first().status:
                        subscription = client.subscription_client.first()
                        subscription.status = paypal_state
                        subscription.save()
        else:
            if not client.subscription_client:
                return return_prices()

    except PermissionDenied:
        # return return_prices()
        pass
    # else:
    #     return return_prices()

    # Если с соглашением о платежах все нормально
    # if subscription.error is not None:
    #     logger.error('Paypal subscription not found: {%s}' % subscription.error)
    # else:
    #     paypal_state = subscription.status.upper()
    #
    #     # Обновляем данные о состоянии оплаты
    #     if paypal_state != client.subscription_client.first().status:
    #         subscription = client.subscription_client.first()
    #         subscription.status = paypal_state
    #         subscription.save()
    # if not client.current_subscription():
    #     return return_prices()

    return False


class IndexView(CachedViewMixin, TemplateView):
    template_name = 'portal/content.html'
    config = None

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            client = get_client(request)

            if client:
                # if client.current_subscription():
                result_subscription = check_subscription(request)

                if not result_subscription:

                    # Если все нормально прошло, то ищем данные для страницы

                    inquiries = Inquiry.objects.filter(client_id=client.id)
                    communications = Communication.objects.filter(client_id=client.id)
                    resources = Resource.objects.filter(client_id=client.id)
                    questions = Question.objects.all()

                    form_inquiry = InquiryForm()

                    plan_disabled = False
                    consultation_disabled = False

                    for t in WasteTime.objects.filter(query__client__client_id=client.id):
                        if t.query.subscription.id == client.current_subscription().id:
                            if t.plan:
                                plan_disabled = True
                            if t.consultation:
                                consultation_disabled = True
                    transactions = Transaction.objects.filter(Q(purchase__subscription__client_id=client.id) | Q(purchase__additional_hours__subscription__client_id=client.id))
                    # SEO
                    title = 'Delano Portal'
                    config = PortalConfig.get_solo()
                    if config:
                        title = config.title or title
                    seo = Seo()
                    seo.set({
                        'title': title,
                    })
                    seo.save(request)
                    return self.render_to_response({
                        'inquiries': inquiries,
                        'communications': communications,
                        'resources': resources,
                        'questions': questions,
                        'client': get_client(request),
                        'new_counts': get_new_counts(client),
                        'form_inquiry': form_inquiry,
                        'prices_link': resolve_url('index') + '#prices',
                        'transactions': transactions,
                        'plan_disabled': plan_disabled,
                        'consultation_disabled': consultation_disabled,
                    })
                else:
                    return result_subscription

                # else:
                #     return return_prices()

            else:
                # Если зашли админы, то отклоняем их. Они не покупают подписки
                # raise PermissionDenied
                raise Http404

        else:
            return redirect('clients:login')
