from django.views.generic.base import View
from .forms import InquiryForm, QueryForm, ExtraForm
from clients.models import Client
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from .models import Inquiry, AnswerInquiry, Communication, Resource, NotificationReceiver
from libs.views_ajax import AjaxViewMixin
from packages.models import Option
from libs.email import send_template


class InquiryView(AjaxViewMixin, View):
    def get(self, request):
        return self.json_response({
            'form_box': self.render_to_string('portal/popups/inquiry.html', {
                'form': InquiryForm()
            }),
        })

    def post(self, request):

        try:
            client = Client.objects.get(user_id=request.user.id)
        except ObjectDoesNotExist:
            return self.json_error()

        form_new = InquiryForm(request.POST)

        if form_new.is_valid():
            inquiry = form_new.save(commit=False)
            inquiry.client_id = client.pk
            inquiry.save()

            return self.json_response({
                'popup': self.render_to_string('portal/popups/submit_inquiry.html')
            })
        else:
            return self.json_error({
                'errors': form_new.error_dict_full
            })


class CountItemsView(AjaxViewMixin, View):
    def post(self, request):
        try:
            client = Client.objects.get(user_id=request.user.id)
        except ObjectDoesNotExist:
            return self.json_error({
                'error': 'Unknown client'
            })

        type_items = request.POST.get('type_items')

        if type_items == 'inquiries':
            inquiries = Inquiry.objects.filter(client_id=client.id)
            answers = AnswerInquiry.objects.filter(inquiry_id__in=inquiries)
            answers.update(
                client_check=True
            )
        elif type_items == 'communications':
            communications = Communication.objects.filter(client_id=client.id)

            communications.update(
                client_check=True
            )
        elif type_items == 'resources':
            resources = Resource.objects.filter(client_id=client.id)
            resources.update(
                client_check=True
            )
        else:
            return self.json_error({
                'error': 'Unknown type update'
            })

        return self.json_response()


class QueryView(AjaxViewMixin, View):
    def get(self, request):

        type_popup = request.GET.get('type_popup')

        if type_popup == 'phone':
            form = QueryForm()
            popup_title = _('Schedule a phone call with a Business Advisor')

        elif type_popup == 'video':
            form = QueryForm()
            popup_title = _('Schedule a video call with a Business Advisor')

        elif type_popup == 'plan':
            form = ExtraForm()
            popup_title = _('Free Project Plan')

            return self.json_response({
                'popup': self.render_to_string('portal/popups/request_consultation.html', {
                    'form': form,
                    'title': popup_title
                })
            })

        elif type_popup == 'consultation':
            form = ExtraForm()
            popup_title = _('Free 45-Minutes Consultation')

            return self.json_response({
                'popup': self.render_to_string('portal/popups/request_consultation.html', {
                    'form': form,
                    'title': popup_title
                })
            })
        else:
            return self.json_error({
                'error': 'Unknown type popup'
            })

        return self.json_response({
            'popup': self.render_to_string('portal/popups/request.html', {
                'form': form,
                'title': popup_title
            })
        })

    def post(self, request):

        type_popup = request.POST.get('type_popup')
        form = QueryForm(request.POST)

        try:
            client = Client.objects.get(user_id=request.user.id)
        except ObjectDoesNotExist:
            return self.json_error({
                'error': 'Unknown client'
            })

        if form.is_valid():
            query_form = form.save(commit=False)
            query_form.client = client

            try:
                option = Option.objects.get(popup=type_popup)
            except ObjectDoesNotExist:
                return self.json_error({
                    'error': 'Unknown type option'
                })

            try:
                query_form.option = option

            except Exception:
                return self.json_error({
                    'error': 'Unknown type popup'
                })
            query_form.subscription = client.current_subscription()
            query_form.save()

            send_template(request, NotificationReceiver.objects.all().values_list('email', flat=True),
                          subject=_('Message from {domain}'),
                          template='portal/mails/query.html',
                          context={
                              'query': query_form,
                          }
                          )

            return self.json_response({
                'popup': self.render_to_string('portal/popups/submit.html', {
                    'time_description': 'Within {} hours you will be notified of the response'.format(client.current_package().hours)
                })
            })

        else:
            return self.json_error({
                'errors': form.error_dict_full
            })
