from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel
from clients.models import Client
from packages.models import Option, Package
from libs.email import send_template
from requests import request
from users.models import AdminUser
from . import options
from config import options as config_options
from subscriptions.models import Subscription
import os
from django.utils.safestring import mark_safe
from django.contrib.sites.shortcuts import get_current_site


class PortalConfig(SingletonModel):

    title = models.CharField(_('title'), max_length=128)
    faq_header = models.CharField(_('header'), max_length=128, blank=True)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('settings')

    def get_absolute_url(self):
        return resolve_url('portal:index')

    def __str__(self):
        return ugettext('portal')


class Question(models.Model):
    question = models.TextField(_('question'))
    answer = models.TextField(_('answer'))

    sort_order = models.PositiveIntegerField(_('order'), default=0)
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('question')
        verbose_name_plural = _('FAQ')
        ordering = ('sort_order', )

    def __str__(self):
        return self.question


class Category(models.Model):
    config = models.ForeignKey(PortalConfig, blank=True, null=True)
    name = models.CharField(_('name'), max_length=64)
    sort_order = models.PositiveIntegerField(_('order'), default=0)

    class Meta:
        verbose_name = _('topic')
        verbose_name_plural = _('topics')
        ordering = ('sort_order', )

    def __str__(self):
        return self.name


class TypeConsultation(models.Model):
    config = models.ForeignKey(PortalConfig, blank=True, null=True)
    name = models.CharField(_('name'), max_length=64)

    class Meta:
        verbose_name = _('type consultation')
        verbose_name_plural = _('types consultations')
        ordering = ('name', )

    def __str__(self):
        return self.name


class Inquiry(models.Model):
    question = models.TextField(_('question'))
    phase = models.CharField(_('phase'), choices=config_options.BUSINESS_LIFE_STAGE, default=config_options.BUSINESS_LIFE_STAGE[0][0], max_length=3)
    category = models.ForeignKey(Category, verbose_name=_('category') )
    client = models.ForeignKey(Client, verbose_name=_('client'), default='', blank=True, null=True)
    created = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('inquiry')
        verbose_name_plural = _('inquiries')
        ordering = ('-created', )

    def __str__(self):
        return self.question

    def answered(self):
        return AnswerInquiry.objects.filter(inquiry_id=self.id).exists()


class AnswerInquiry(models.Model):
    inquiry = models.OneToOneField(Inquiry, verbose_name=_('inquiry'), related_name='answer_inquiry')
    manager = models.ForeignKey(AdminUser, verbose_name=_('manager'), blank=True, null=True)
    answer = models.TextField(_('answer'))
    client_check = models.BooleanField(_('client check'), default=False)
    send = models.BooleanField(_('send'), default=False, )
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('inquiry')
        verbose_name_plural = _('inquiries')

    def save(self, *args, **kwargs):
        email = self.inquiry.client.user.email
        p1 = 'https' if os.environ.get("HTTPS") == "on" else 'http'
        p2 = resolve_url('portal:index')

        if email:
            send_template(request, email,
                          subject=_('Message from {domain}'),
                          template='portal/mails/inquiry.html',
                          context={
                              'name': self.inquiry.client.user.get_full_name(),
                              'question': self.inquiry.question,
                              'answer': self.answer,
                              'portal': mark_safe('{}://{}{}'.format(p1, get_current_site(request).domain, p2)),
                          }
                          )
            self.send = True
        super(AnswerInquiry, self).save(request, *args, **kwargs)

    def __str__(self):
        return self.inquiry.question


class Resource(models.Model):
    title = models.CharField(_('title'), max_length=128)
    url = models.URLField(_('URL'))
    client = models.ForeignKey(Client, verbose_name=_('client'))
    client_check = models.BooleanField(_('client check'), default=False)

    updated = models.DateTimeField(_('change date'), auto_now=True)
    sort_order = models.PositiveIntegerField(_('order'), default=0)

    class Meta:
        verbose_name = _('resource')
        verbose_name_plural = _('resources')
        ordering = ('sort_order', )

    def __str__(self):
        return self.title


class Communication(models.Model):
    title = models.CharField(_('title'), max_length=255)
    summary = models.TextField(_('summary'))
    client = models.ForeignKey(Client, verbose_name=_('client'))
    client_check = models.BooleanField(_('client check'), default=False)

    created = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('communication')
        verbose_name_plural = _('communications')
        ordering = ('-created', )

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        email = self.client.user.email

        p1 = 'https' if os.environ.get("HTTPS") == "on" else 'http'
        p2 = resolve_url('portal:index')

        if  email:
            send_template(request, email,
                          subject=_('Message from {domain}'),
                          template='portal/mails/communication.html',
                          context={
                              'portal': mark_safe('{}://{}{}'.format(p1, get_current_site(request).domain, p2)),
                              'name': self.client.user.get_full_name(),
                          }
                          )
            self.send = True
        super(Communication, self).save(*args, **kwargs)


class Query(models.Model):
    option = models.ForeignKey(Option, verbose_name=_('option'), )
    client = models.ForeignKey(Client, verbose_name=_('client'), related_name='query_client')
    date = models.DateField(_('date'), )
    time = models.PositiveSmallIntegerField(_('time'), choices=options.TIME_OPTIONS, default=options.TIME_AM_0002)
    description = models.TextField(_('description'), )
    industry = models.CharField(_('industry'), choices=config_options.INDUSTRY, default=config_options.INDUSTRY[0][0], blank=True, null=True, max_length=3)
    type_consultation = models.ForeignKey(TypeConsultation, verbose_name=_('type consultation'), blank=True, null=True, )
    subscription = models.ForeignKey(Subscription, verbose_name=_('subscription'), default='', related_name='query_subscription', null=True)
    created = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('query')
        verbose_name_plural = _('queries')
        ordering = ('-created', )

    def __str__(self):
        return self.description

    def wt(self):
        try:
            return WasteTime.objects.get(query_id=self.id)
        except:
            return None


class NotificationReceiver(models.Model):
    email = models.EmailField(_('e-mail'), )
    config = models.ForeignKey(PortalConfig, )

    class Meta:
        verbose_name = _('e-mail')
        verbose_name_plural = _('e-mails')


class WasteTime(models.Model):
    query = models.OneToOneField(Query, verbose_name=_('query'), related_name='time_query')
    hours = models.PositiveSmallIntegerField(_('hours'), default=1 )
    manager = models.ForeignKey(AdminUser, verbose_name=_('manager'))
    plan = models.BooleanField(_('Free Project Plan'), default=False, )
    consultation = models.BooleanField(_('Free Consultation'), default=False, )

    created = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('waste of time')
        verbose_name_plural = _('wastes of time')
        ordering = ('-created', )

    def __str__(self):
        return str(self.hours) + ' hour(s)'
