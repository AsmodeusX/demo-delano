from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Config(AppConfig):
    name = 'portal'
    verbose_name = _('Portal')

    def ready(self):
        from django.shortcuts import resolve_url
        from libs.js_storage import JS_STORAGE

        JS_STORAGE.update({
            'portal': resolve_url('portal:index'),
            'ajax_query': resolve_url('portal:ajax_query'),
            'ajax_inquiry': resolve_url('portal:ajax_inquiry'),
            'ajax_count_items': resolve_url('portal:ajax_count_items'),
        })
