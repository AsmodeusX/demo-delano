(function($) {
    var inquiryMsgVisible = false,
        timeCountZero = 3000;

    //accordion in tabs
    var accordionElements = $('.accordion');
    accordionElements.on('click', function(){
        var el = $(this);
        $('.accordion-inner').slideUp();
        if(!el.hasClass('opened')) {
            el.parent().find('.accordion-inner').slideDown();
            accordionElements.removeClass('opened');
            $(this).addClass('opened');
        } else {
            accordionElements.removeClass('opened');
        }
    });

    //tooltip

    $('.controls-hours .controls-link').tooltip({
        position: 'bottom'
    });

    $(window).on('load resize', function() {
        var h = 0;
        if ($.winWidth() >= 1024) {
            h = $.winHeight() - ($('.portal-header').outerHeight() + $('.portal-header-tab').eq(0).outerHeight() + $('.portal-footer').outerHeight() + $('.btn-green-portal').innerHeight());
            $('.content-inner-wrapper').css('height', h + 'px');
        } else {
            h = $.winHeight() - ($('.portal-mobile-header').innerHeight() + $('.portal-footer').outerHeight());
            $('.content-inner-wrapper').css('height', h + 'px');
        }
    });

    $(window).on('load', function () {
        $('.sticky').sticky();
    });

    $(document).ready(function () {

        $('#open-send-inquiry').on('click', function (e) {

            if ($.winWidth() >= 1200) {
                var $msgBlock = $('#inquiry-msg');

                $msgBlock.addClass('up');
                $msgBlock.removeClass('down');

                inquiryMsgVisible = true;
            } else {
                $.ajax({
                    url: window.js_storage.ajax_inquiry,
                    type: 'GET',
                    dataType: 'json',
                    success: function(response) {
                        $.popup({
                            classes: 'contact-popup inquiry-popup',
                            content: response.form_box
                        }).show();
                    },
                    error: $.parseError(function() {
                        alert(window.DEFAULT_AJAX_ERROR);
                    })
                });
            }

        });

        $('.tab-inquiries, .tab-communications, .tab-resources').on('click', function () {
            var $that = $(this);
            setTimeout(function () {
                return $.ajax({
                    url: window.js_storage.ajax_count_items,
                    type: 'POST',
                    data: {
                        'type_items': $that.data('tab')
                    },
                    dataType: 'json',
                    success: function(response) {

                        $that.find('.tab-counter').remove();

                    },
                    error: $.parseError(function() {
                        // alert(window.DEFAULT_AJAX_ERROR);
                    })
                });
            }, timeCountZero)
        });

        $('#content-wrapper').on('click', function (e){ // событие клика по веб-документу
            var div = $("#inquiry-msg"); // тут указываем ID элемента
            if ((e.target || e.srcElement).id === 'open-send-inquiry') return false;
            if (!div.is(e.target) // если клик был не по нашему блоку
                && div.has(e.target).length === 0) { // и не по его дочерним элементам
                if (inquiryMsgVisible) {
                    setTimeout(function () {
                        var $block = $('#inquiry-msg');
                        $block.addClass('down');
                        $block.removeClass('up');
                        inquiryMsgVisible = false;
                    }, 500);
                }
            }
        });

        initSelect();

    });

    $(document).on('submit', '#inquiry', function (e) {
        e.preventDefault();
        var $form = $($('#inquiry'));

        return $.ajax({
            url: window.js_storage.ajax_inquiry,
            type: 'POST',
            data: $form.serialize(),
            dataType: 'json',
            beforeSend: function () {
                $form.find('.field').removeClass('invalid');
            },
            success: function(response) {
                // $('.inquiries').prepend(response.inquiry);
                if (response.popup) {
                    $.popup({
                        classes: 'query-popup success-query-popup',
                        content: response.popup
                    }).show();
                }
                var $block = $('#inquiry-msg');
                $block.addClass('down');
                $block.removeClass('up');

                setTimeout(function () {
                    $block.find('form').get(0).reset();
                }, 750);
                inquiryMsgVisible = false;
            },
            error: $.parseError(function(response) {

                if (response.errors) {
                    response.errors.forEach(function(record) {
                        var $field = $form.find('.' + record.fullname);
                        $field.addClass(record.class);
                    })
                } else {
                    alert(window.DEFAULT_AJAX_ERROR);
                }
            })
        });
    });

    function initSelect(){
        var $form = $('form');
        $form.find('select').each(function() {
            var $select = $(this);
            var $control = $select.closest('.control');

            $select.selectmenu({
                appendTo: $control,
                select: function( event, ui ) {
                    // красим выбранный пункт из селекта в обычный цвет шрифта формы после выбора
                    $control.find('.ui-selectmenu-text').css({
                        'color': '#35363a',
                        'font-weight': 'bold'
                    });
                }
            });

            $control.css('position', 'relative');
        });
    }

})(jQuery);

