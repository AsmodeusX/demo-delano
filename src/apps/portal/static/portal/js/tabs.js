(function($) {
    window.content_tabs = [];

    $(document).ready(function() {
        var $contents = $('.page-content');

        for (var i = 0; i < $contents.length; i++) {
            window.content_tabs.push(0);
        }

        $contents.each(function (index_content) {
            var $tabs =$('.tabs-list .tab');

            $tabs.each(function (index_tab) {
                var $tab = $(this);

                $tab.on('click', function () {
                    $tabs.removeClass('active');
                    // $tab.addClass('active');
                    var t = $tab.data('tab');
                    $('.tab[data-tab=' + t + ']').addClass('active');

                    setTab($tab.data('tab'));
                    updateTab(index_content, index_tab);
                    window.main_menu.close();
                });
            });

            $('.nav-link').on('click', function () {
                window.main_menu.close();
            });
            
            var tab = getAllUrlParams().tab,
                $tab = $('.tab[data-tab=' + tab + ']');

            if ($tab.length) {
                $tab.trigger('click');
            } else {
                $tabs.eq(0).trigger('click');
            }

        })
    });

    function setTab(tab) {
        var $tabs = $('.content-inner-wrapper .content-inner');

        $tabs.closest('.tab-content').hide();

        $tabs.each(function () {
            var $tab = $(this);

            if ($tab.data('tab') === tab)
                $tab.closest(".tab-content").show();
        });
    }

    function updateTab(content_index, tab_index) {
        window.content_tabs[content_index] = tab_index;
    }


    // Получение GET параметров
    function getAllUrlParams(url) {

        // извлекаем строку из URL или объекта window
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

        // объект для хранения параметров
        var obj = {};

        // если есть строка запроса
        if (queryString) {

            // данные после знака # будут опущены
            queryString = queryString.split('#')[0];

            // разделяем параметры
            var arr = queryString.split('&');

            for (var i=0; i<arr.length; i++) {
                // разделяем параметр на ключ => значение
                var a = arr[i].split('=');

                // обработка данных вида: list[]=thing1&list[]=thing2
                var paramNum = undefined;
                var paramName = a[0].replace(/\[\d*\]/, function(v) {
                    paramNum = v.slice(1,-1);
                    return '';
                });

                // передача значения параметра ('true' если значение не задано)
                var paramValue = typeof(a[1])==='undefined' ? true : a[1];

                // преобразование регистра
                paramName = paramName.toLowerCase();
                paramValue = paramValue.toLowerCase();

                // если ключ параметра уже задан
                if (obj[paramName]) {
                    // преобразуем текущее значение в массив
                    if (typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                    }
                    // если не задан индекс...
                    if (typeof paramNum === 'undefined') {
                        // помещаем значение в конец массива
                        obj[paramName].push(paramValue);
                    }
                    // если индекс задан...
                    else {
                        // размещаем элемент по заданному индексу
                        obj[paramName][paramNum] = paramValue;
                    }
                }
                // если параметр не задан, делаем это вручную
                else {
                    obj[paramName] = paramValue;
                }
            }
        }

        return obj;
    }

})(jQuery);
