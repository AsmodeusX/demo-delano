(function ($) {
    var type_popup;

    $(document).ready(function () {
        setNavItems();

        $('#current-package').on('click', function () {
            $.ajax({
                url: window.js_storage.ajax_package,
                type: 'GET',
                dataType: 'json',
                data: data,

                success: function(response) {
                    if (response.popup) {
                        $.popup({
                            classes: 'package-popup',
                            content: response.popup
                        }).show();
                    }
                },
                error: $.parseError(function() {
                    alert(window.DEFAULT_AJAX_ERROR);
                })
            });
        });
    });
    setChangeBackground();

    function setChangeBackground() {
        $('.change-background-button').on('click', function () {
            $.ajax({
                url: window.js_storage.ajax_change_bg,
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    $.popup({
                        classes: 'profile-change-bg',
                        content: response.popup
                    }).show()
                },
                error: $.parseError(function () {
                    alert(window.DEFAULT_AJAX_ERROR);
                })
            });
        });

    }
    function setNavItems() {

        $('.nav-item').on('click', function () {
            var $link = $(this).find('.nav-link');

            if (!($link.attr('href'))) {
                type_popup = $link.data('popup');
                openQueryPopup(type_popup);
            }
        });
    }
    function openQueryPopup(type_popup) {
        $.ajax({
            url: window.js_storage.ajax_query,
            type: 'GET',
            dataType: 'json',
            data: {
                'type_popup': type_popup
            },
            success: function(response) {

                if (response.popup) {
                    $.popup({
                        classes: 'query-popup',
                        content: response.popup
                    }).on('after_show', function () {
                        initSelect();
                        initDatepicker();
                    }).show();
                }
            },
            error: $.parseError(function() {
                alert(window.DEFAULT_AJAX_ERROR);
            })
        });
    }
    function initDatepicker(){
        var $form = $('.popup-content form');
        $form.find('.datepicker').each(function() {
            var $datepicker = $(this);
            var $control = $form.closest('.control');

            $datepicker.datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: 'c:+100',
                altFormat: "yy-mm-dd",
                beforeShow: function () {
                    $('.query-popup .calendar-wrap').append($('#ui-datepicker-div'));
                }
            });

            $control.css('position', 'relative');
        });
    }
    function initSelect(){
        var $form = $('.popup-content form');
        $form.find('select').each(function() {
            var $select = $(this);
            var $control = $select.closest('.control');

            $select.selectmenu({
                appendTo: $control,
                select: function( event, ui ) {
                    // красим выбранный пункт из селекта в обычный цвет шрифта формы после выбора
                    $control.find('.ui-selectmenu-text').css({
                        'color': '#35363a',
                        'font-weight': 'bold'
                    });
                }
            });

            $control.css('position', 'relative');
        });
    }

    $(document).on('click', '#send-query', function () {

            var $form = $('#query-popup-form');
            var data = $form.serializeArray();
            data.push({
                name: 'type_popup',
                value: type_popup
            });

            $.ajax({
                url: window.js_storage.ajax_query,
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function () {
                    $form.find('.field').removeClass('invalid');
                },
                success: function(response) {

                    if (response.popup) {
                        $.popup({
                            classes: 'query-popup success-query-popup',
                            content: response.popup
                        }).show();
                    }
                },
                error: $.parseError(function(response) {

                    if (response.errors) {
                        response.errors.forEach(function(record) {
                            var $field = $form.find('.' + record.fullname);
                            $field.addClass(record.class);
                        })
                    } else {
                        alert(window.DEFAULT_AJAX_ERROR);
                    }
                })
            });
            return false;
        });
    $(document).on('click', '.background-img', function (e) {
        $.ajax({
            url: window.js_storage.ajax_change_bg,
            type: 'POST',
            data: {
                'background': $(e.currentTarget).data('img')
            },
            dataType: 'json',
            success: function(response) {
                $('.actions-nav').remove();
                $('.sidebar').append(response.background);
                setChangeBackground();
                setNavItems();
            },
            error: $.parseError(function () {
                alert(window.DEFAULT_AJAX_ERROR);
            })
        });
    });
})(jQuery);