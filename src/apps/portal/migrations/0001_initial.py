# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0001_initial'),
        ('packages', '0001_initial'),
        ('subscriptions', '0001_initial'),
        ('users', '0003_auto_20191021_0423'),
    ]

    operations = [
        migrations.CreateModel(
            name='PortalConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('header', models.TextField(verbose_name='page header')),
                ('text_field', ckeditor.fields.CKEditorField(verbose_name='page text content')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'verbose_name': 'settings',
            },
        ),


    ]
