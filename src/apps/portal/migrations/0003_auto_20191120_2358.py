# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0002_auto_20191023_0615'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='portalconfig',
            name='communications_header',
        ),
        migrations.RemoveField(
            model_name='portalconfig',
            name='inquiry_header',
        ),
        migrations.RemoveField(
            model_name='portalconfig',
            name='payments_header',
        ),
        migrations.RemoveField(
            model_name='portalconfig',
            name='resources_header',
        ),
    ]
