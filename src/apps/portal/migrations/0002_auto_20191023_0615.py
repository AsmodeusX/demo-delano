# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnswerInquiry',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('answer', models.TextField(verbose_name='answer')),
                ('client_check', models.BooleanField(default=False, verbose_name='client check')),
                ('send', models.BooleanField(default=False, verbose_name='send')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'verbose_name_plural': 'inquiries',
                'verbose_name': 'inquiry',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='name')),
                ('sort_order', models.PositiveIntegerField(default=0, verbose_name='order')),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name_plural': 'topics',
                'verbose_name': 'topic',
            },
        ),
        migrations.CreateModel(
            name='Communication',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('summary', models.TextField(verbose_name='summary')),
                ('client_check', models.BooleanField(default=False, verbose_name='client check')),
                ('created', models.DateTimeField(auto_now=True, verbose_name='change date')),
                ('client', models.ForeignKey(to='clients.Client', verbose_name='client')),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name_plural': 'communications',
                'verbose_name': 'communication',
            },
        ),
        migrations.CreateModel(
            name='Inquiry',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('question', models.TextField(verbose_name='question')),
                ('phase', models.CharField(
                    choices=[('SU', 'Start up'), ('TN', 'Transition'), ('MT', 'Maturity'), ('EN', 'Expansion'),
                             ('GT', 'Growth')], default='SU', max_length=3, verbose_name='phase')),
                ('created', models.DateTimeField(auto_now=True, verbose_name='change date')),
                ('category', models.ForeignKey(to='portal.Category', verbose_name='category')),
                ('client',
                 models.ForeignKey(null=True, verbose_name='client', default='', to='clients.Client', blank=True)),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name_plural': 'inquiries',
                'verbose_name': 'inquiry',
            },
        ),
        migrations.CreateModel(
            name='NotificationReceiver',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('email', models.EmailField(max_length=254, verbose_name='e-mail')),
            ],
            options={
                'verbose_name_plural': 'e-mails',
                'verbose_name': 'e-mail',
            },
        ),
        migrations.DeleteModel(name='PortalConfig'),
        migrations.CreateModel(
            name='PortalConfig',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=128, verbose_name='title')),
                ('inquiry_header', models.CharField(blank=True, max_length=128, verbose_name='header')),
                ('communications_header', models.CharField(blank=True, max_length=128, verbose_name='header')),
                ('resources_header', models.CharField(blank=True, max_length=128, verbose_name='header')),
                ('faq_header', models.CharField(blank=True, max_length=128, verbose_name='header')),
                ('payments_header', models.CharField(blank=True, max_length=128, verbose_name='header')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'verbose_name': 'settings',
            },
        ),
        migrations.CreateModel(
            name='Query',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('date', models.DateField(verbose_name='date')),
                ('time', models.PositiveSmallIntegerField(
                    choices=[(1, '00:00AM - 02:00AM'), (2, '02:00AM - 04:00AM'), (3, '04:00AM - 06:00AM'),
                             (4, '06:00AM - 08:00AM'), (5, '08:00AM - 10:00AM'), (6, '10:00AM - 12:00AM'),
                             (7, '00:00PM - 02:00PM'), (8, '02:00PM - 04:00PM'), (9, '04:00PM - 06:00PM'),
                             (10, '06:00PM - 08:00PM'), (11, '08:00PM - 10:00PM'), (12, '10:00PM - 12:00PM')],
                    default=1, verbose_name='time')),
                ('description', models.TextField(verbose_name='description')),
                ('industry', models.CharField(
                    choices=[('AI', 'Agricultural industry'), ('AR', 'Arms industry'), ('AU', 'Automotive industry'),
                             ('BC', 'Broadcasting Industry'), ('CH', 'Chemical industry'),
                             ('CL', 'Clothing & apparel industry'), ('CT', 'Computer industry'),
                             ('CN', 'Construction industry'), ('DR', 'Direct selling industry'),
                             ('DB', 'Distribution industry'), ('ED', 'Education industry'),
                             ('EL', 'Electronics industry'), ('EN', 'Entertainment industry'), ('FM', 'Film industry'),
                             ('FS', 'Financial services industry'), ('FN', 'Fishing industry'), ('FD', 'Food industry'),
                             ('HC', 'Health care industry'), ('HS', 'Hospitality industry'),
                             ('II', 'Information industry'), ('MA', 'Manufacturing'), ('MM', 'Mass media industry'),
                             ('MS', 'Music industry'), ('NM', 'News media industry'), ('OP', 'Other products industry'),
                             ('OS', 'Other services industry'), ('PU', 'Publishing industry'),
                             ('SF', 'Software industry'), ('TL', 'Telecommunications industry'),
                             ('TR', 'Transportation industry'), ('UU', 'Unknown/unsure')], null=True, max_length=3,
                    verbose_name='industry', default='AI', blank=True)),
                ('created', models.DateTimeField(auto_now=True, verbose_name='change date')),
                ('client', models.ForeignKey(related_name='query_client', verbose_name='client', to='clients.Client')),
                ('option', models.ForeignKey(to='packages.Option', verbose_name='option')),
                ('subscription',
                 models.ForeignKey(null=True, verbose_name='subscription', default='', to='subscriptions.Subscription',
                                   related_name='query_subscription')),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name_plural': 'queries',
                'verbose_name': 'query',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('question', models.TextField(verbose_name='question')),
                ('answer', models.TextField(verbose_name='answer')),
                ('sort_order', models.PositiveIntegerField(default=0, verbose_name='order')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name_plural': 'FAQ',
                'verbose_name': 'question',
            },
        ),
        migrations.CreateModel(
            name='Resource',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=128, verbose_name='title')),
                ('url', models.URLField(verbose_name='URL')),
                ('client_check', models.BooleanField(default=False, verbose_name='client check')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
                ('sort_order', models.PositiveIntegerField(default=0, verbose_name='order')),
                ('client', models.ForeignKey(to='clients.Client', verbose_name='client')),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name_plural': 'resources',
                'verbose_name': 'resource',
            },
        ),
        migrations.CreateModel(
            name='TypeConsultation',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='name')),
                ('config', models.ForeignKey(null=True, to='portal.PortalConfig', blank=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name_plural': 'types consultations',
                'verbose_name': 'type consultation',
            },
        ),
        migrations.CreateModel(
            name='WasteTime',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('hours', models.PositiveSmallIntegerField(default=1, verbose_name='hours')),
                ('plan', models.BooleanField(default=False, verbose_name='Free Project Plan')),
                ('consultation', models.BooleanField(default=False, verbose_name='Free Consultation')),
                ('created', models.DateTimeField(auto_now=True, verbose_name='change date')),
                ('manager', models.ForeignKey(to='users.AdminUser', verbose_name='manager')),
                ('query', models.ForeignKey(related_name='time_query', verbose_name='query', to='portal.Query')),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name_plural': 'wastes of time',
                'verbose_name': 'waste of time',
            },
        ),
        migrations.AddField(
            model_name='query',
            name='type_consultation',
            field=models.ForeignKey(null=True, verbose_name='type consultation', to='portal.TypeConsultation',
                                    blank=True),
        ),
        migrations.AddField(
            model_name='notificationreceiver',
            name='config',
            field=models.ForeignKey(to='portal.PortalConfig'),
        ),
        migrations.AddField(
            model_name='category',
            name='config',
            field=models.ForeignKey(null=True, to='portal.PortalConfig', blank=True),
        ),
        migrations.AddField(
            model_name='answerinquiry',
            name='inquiry',
            field=models.OneToOneField(related_name='answer_inquiry', verbose_name='inquiry', to='portal.Inquiry'),
        ),
        migrations.AddField(
            model_name='answerinquiry',
            name='manager',
            field=models.ForeignKey(null=True, verbose_name='manager', to='users.AdminUser', blank=True),
        ),
        migrations.AlterField(
            model_name='wastetime',
            name='query',
            field=models.OneToOneField(verbose_name='query', to='portal.Query', related_name='time_query'),
        ),
    ]
