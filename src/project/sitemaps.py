from django.contrib.sitemaps import GenericSitemap
from main.models import MainPageConfig
from politics.models import Privacy, Terms

mainpage = {
    'queryset': MainPageConfig.objects.all(),
    'date_field': 'updated',
}

privacypage = {
    'queryset': Privacy.objects.all(),
    'date_field': 'updated',
}

termspage = {
    'queryset': Terms.objects.all(),
    'date_field': 'updated',
}


site_sitemaps = {
    'main': GenericSitemap(mainpage, changefreq='daily', priority=1),
    'privacy': GenericSitemap(privacypage, changefreq='daily', priority=1),
    'terms': GenericSitemap(termspage, changefreq='daily', priority=1),
}
